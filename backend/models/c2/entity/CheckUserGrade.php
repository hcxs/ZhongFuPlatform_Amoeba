<?php
/**
 * Created by PhpStorm.
 * User: jerrygo
 * Date: 19-6-21
 * Time: 下午9:26
 */

namespace backend\models\c2\entity;


use common\models\c2\entity\AmoebaFormItem;
use common\models\c2\entity\UserGrade;
use common\models\c2\statics\UserGradeState;
use Yii;
use yii\helpers\ArrayHelper;

class CheckUserGrade extends UserGrade
{
    public $amoeba_name;
    public $join_username;
    public $invite_username;
    public $checker_name;
    public $is_pass;

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'amoeba_name' => Yii::t('app.c2', 'Amoeba Name'),
            'join_username' => Yii::t('app.c2', 'Join Username'),
            'invite_username' => Yii::t('app.c2', 'Invite Username'),
        ]); // TODO: Change the autogenerated stub
    }

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['dues', 'amoeba_form_id', 'check_by', 'is_pass'], 'required'],
        ]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
        if ($this->is_pass == 1) {
            $model = new AmoebaFormItem();
            $model->loadDefaultValues();
            $model->user_id = $this->join_user_id;
            $model->amoeba_form_id = $this->amoeba_form_id;
            $model->amoeba_id = $this->amoeba_id;
            if ($model->save()) {
                $this->updateAttributes(['state' => UserGradeState::STATE_PASS]);
            }
        }
    }
}