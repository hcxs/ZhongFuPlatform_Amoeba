<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/19 0019
 * Time: 下午 18:01
 */

namespace backend\models\c2\entity;


use common\models\c2\entity\Amoeba;
use common\models\c2\entity\AmoebaGroup;

class DefaultAmoeba extends Amoeba
{
    public function beforeDelete()
    {
        foreach ($this->getAmoebaForms()->all() as  $item) {
            $item->delete();
        }
        foreach ($this->getAmoebaFormItems()->all() as $item) {
            $item->delete();
        }
        foreach ($this->getAmoebaGroups()->all() as $item) {
            $item->childrenAmoeba->delete();
            $item->delete();
        }
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
        $attributes = [
            'amoeba_id' => $this->id,
            'children_amoeba_id' => $this->id,
            'parent_amoeba_id' => null,
        ];
        $model = new AmoebaGroup();
        $model->setAttributes($attributes);
        $model->save();
    }

}