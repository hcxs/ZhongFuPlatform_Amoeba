<?php

use cza\base\widgets\ui\common\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use cza\base\models\statics\EntityModelStatus;
use cza\base\models\statics\OperationEvent;

/* @var $this yii\web\View */
/* @var $searchModel common\models\c2\search\CmsBlock */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app.c2', 'Cms AlbumBlocks');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="well cms-block-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => Yii::t('app.c2', 'Showing <b>{begin, number}-{end, number}</b> of <b>{totalCount, number}</b> {totalCount, plural, one{{item}} other{{items}}}.'),
        'pjax' => true,
        'hover' => true,
        'showPageSummary' => true,
        'panel' => ['type' => GridView::TYPE_PRIMARY, 'heading' => Yii::t('app.c2', 'Items')],
        'toolbar' => [
            [
                'content' => yii\bootstrap\ButtonDropdown::widget([
                    'label' => "<i class='glyphicon glyphicon-plus'></i> " . Yii::t("app.c2", "Create"),
                    'encodeLabel' => false,
                    'options' => [
                        'class' => 'btn btn-success',
                    ],
                    'dropdown' => [
                        'items' => [
                            ['label' => Yii::t('app.c2', "Cms AlbumBlock"), 'url' => ['edit'], 'linkOptions' => []],
                            ['label' => Yii::t('app.c2', "Cms AlbumLayoutBlock"), 'url' => ['album-layout/edit'], 'linkOptions' => []],
                        ],
                    ],
                ]) .
                Html::button('<i class="glyphicon glyphicon-remove"></i>', [
                    'class' => 'btn btn-danger',
                    'title' => Yii::t('app.c2', 'Delete Selected Items'),
                    'onClick' => "jQuery(this).trigger('" . OperationEvent::DELETE_BY_IDS . "', {url:'" . Url::toRoute('multiple-delete') . "'});",
                ]) . ' ' .
                Html::a('<i class="glyphicon glyphicon-repeat"></i>', Url::current(), [
                    'class' => 'btn btn-default',
                    'title' => Yii::t('app.c2', 'Reset Grid')
                ]),
            ],
            '{export}',
            '{toggleData}',
        ],
        'exportConfig' => [],
        'columns' => [
            ['class' => 'kartik\grid\CheckboxColumn'],
//            ['class' => 'kartik\grid\SerialColumn'],
//            [
//                'class' => 'kartik\grid\ExpandRowColumn',
//                'expandIcon' => '<span class="fa fa-plus-square-o"></span>',
//                'collapseIcon' => '<span class="fa fa-minus-square-o"></span>',
//                'detailUrl' => Url::toRoute(['detail']),
//                'value' => function ($model, $key, $index, $column) {
//                    return GridView::ROW_COLLAPSED;
//                },
//            ],
//            'id',
            [
                'attribute' => 'code',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a($model->code, [$model->type == \common\models\c2\statics\CmsBlockType::TYPE_ALBUM_LAYOUT ? 'album-layout/edit' : 'edit', 'id' => $model->id], ['data-pjax' => '0']);
                }
            ],
            [
                'attribute' => 'label',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a($model->label, [$model->type == \common\models\c2\statics\CmsBlockType::TYPE_ALBUM_LAYOUT ? 'album-layout/edit' : 'edit', 'id' => $model->id], ['data-pjax' => '0']);
                }
            ],
            [
                'attribute' => 'type',
                'value' => function($model) {
                    return \common\models\c2\statics\CmsBlockType::getLabel($model->type);
                }
            ],
//            'content:ntext',
            // 'created_by',
            // 'updated_by',
            // 'status',
            // 'position',
            // 'created_at',
            'updated_at',
            [
                'attribute' => 'status',
                'class' => '\kartik\grid\EditableColumn',
                'editableOptions' => [
                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    'formOptions' => ['action' => Url::toRoute('editColumn')],
                    'data' => EntityModelStatus::getHashMap('id', 'label'),
                    'displayValueConfig' => EntityModelStatus::getHashMap('id', 'label'),
                ],
                'filter' => EntityModelStatus::getHashMap('id', 'label'),
                'value' => function($model) {
                    return $model->getStatusLabel();
                }
            ],
            [
                'class' => '\common\widgets\grid\ActionColumn',
                'template' => ' {view} {update} {delete}',
                'buttons' => [
//                    'itemlist' => function ($url, $model, $key) {
//                        return Html::a('<span class="glyphicon glyphicon-th-list"></span>', ['/cms/block/album', 'pid' => $model->id], [
//                            'title' => Yii::t('app.c1', 'Sub Cate'),
//                            'data-pjax' => '0',
//                        ]);
//                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [$model->type == \common\models\c2\statics\CmsBlockType::TYPE_ALBUM_LAYOUT ? 'album-layout/edit' : 'edit', 'id' => $model->id], [
                                    'title' => Yii::t('app', 'Info'),
                                    'data-pjax' => '0'
                        ]);
                    }
                ]
            ],
        ],
    ]);
    ?>

</div>
