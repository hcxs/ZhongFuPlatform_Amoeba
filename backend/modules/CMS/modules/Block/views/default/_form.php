<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use cza\base\widgets\ui\adminlte2\InfoBox;
use cza\base\models\statics\EntityModelStatus;

$messageName = $model->getMessageName();
?>

<?php
$form = ActiveForm::begin([
    'action' => ['edit', 'id' => $model->id],
    'options' => [
        'id' => $model->getBaseFormName(),
        'data-pjax' => true,
    ]]);
?>

<div class="<?= $model->getPrefixName('form') ?>">
    <?php if (Yii::$app->session->hasFlash($messageName)): ?>
        <?php
        if (!$model->hasErrors()) {
            echo InfoBox::widget([
                'withWrapper' => false,
                'messages' => Yii::$app->session->getFlash($messageName),
            ]);
        } else {
            echo InfoBox::widget([
                'defaultMessageType' => InfoBox::TYPE_WARNING,
                'messages' => Yii::$app->session->getFlash($messageName),
            ]);
        }
        ?>
    <?php endif; ?>
    <?php
    echo Html::beginTag('div', ['class' => 'box-footer']);
    echo Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('app.c2', 'Save'), ['type' => 'button', 'class' => 'btn btn-primary pull-right']);
    echo Html::a('<i class="fa fa-arrow-left"></i> ' . Yii::t('app.c2', 'Go Back'), ['index'], ['data-pjax' => '0', 'class' => 'btn btn-default pull-right', 'title' => Yii::t('app.c2', 'Go Back'),]);
    echo Html::endTag('div');
    ?>
    <div class="well">

        <div class="kv-avatar center-block text-center" style="width:230px">

            <?php
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 1,
                'attributes' => [
                    'avatar' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => '\kartik\widgets\FileInput',
                        'options' => [
                            'options' => [
                                'accept' => 'image/*',
                            ],
                            'pluginOptions' => [
                                'overwriteInitial' => true,
                                'maxFileSize' => 1500,
                                'showClose' => false,
                                'showCaption' => false,
                                'browseLabel' => '',
                                'removeLabel' => '',
                                'browseIcon' => '<i class="glyphicon glyphicon-folder-open"></i>',
                                'removeIcon' => '<i class="glyphicon glyphicon-remove"></i>',
                                'removeTitle' => 'Cancel or reset changes',
                                'elErrorContainer' => '#kv-avatar-errors-1',
                                'msgErrorClass' => 'alert alert-block alert-danger',
                                'defaultPreviewContent' => '<img src="/images/common/default_img.png" alt="' . Yii::t('app.c2', '{s1} avatar', ['s1' => Yii::t('app.c2', 'Product')]) . '" style="width:160px">',
                                'layoutTemplates' => "{main2: '{preview} {browse} {remove}'}",
                                'allowedFileExtensions' => ["jpg", "png", "gif"],
                                'showUpload' => false,
                                'initialPreview' => $model->getInitialPreview('avatar', \cza\base\models\statics\ImageSize::ORGINAL),
                                'initialPreviewConfig' => $model->getInitialPreview('avatar'),
                            ],
                        ],
                    ],
                ]
            ]);
            ?>
        </div>
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [
                'type' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => \common\models\c2\statics\CmsBlockType::getHashMap('id', 'label')],
                'code' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('code')]],
                'label' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('label')]],
                'status' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => EntityModelStatus::getHashMap('id', 'label')],
                'position' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\touchspin\TouchSpin', 'options' => [
                    'pluginOptions' => [
                        'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>',
                        'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>',
                    ],
                ],],
            ]
        ]);

        echo '引用其他内容块标签， 如：<br/>{{internal:SEO_CODE}} SEO_CODE CmsPage表的SEO_CODE字段，根据该字段获取对应文章 <br/>
        {{block:CODE}}   CODE 内容块的编码，根据code 获取指定内容块的content，并插入文章<br/>
        {{album_block:CODE}}  CODE 相册的编码，根据code 获取指定相册块的轮播图集，并插入文章<br/>
        {{widget:<WidgetPath>|params:{p1=xx&p2=xx}}}，挂件Widget调用方式 <br/>';

        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                'content' => ['type' => Form::INPUT_WIDGET,
                    'widgetClass' => 'common\widgets\ueditor\Ueditor',
                    'options' => [
                        'class' => 'col-sm-12',
                    ],],
            ],
        ]);

        echo Html::beginTag('div', ['class' => 'box-footer']);
        echo Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('app.c2', 'Save'), ['type' => 'button', 'class' => 'btn btn-primary pull-right']);
        echo Html::a('<i class="fa fa-arrow-left"></i> ' . Yii::t('app.c2', 'Go Back'), ['index'], ['data-pjax' => '0', 'class' => 'btn btn-default pull-right', 'title' => Yii::t('app.c2', 'Go Back'),]);
        echo Html::endTag('div');
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
