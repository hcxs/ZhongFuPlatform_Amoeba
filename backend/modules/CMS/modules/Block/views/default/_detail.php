<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


?>
<div class="cms-block-detail">

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'id',
            'type',
            'code',
            'label',
            'url:url',
            'content:ntext',
            'created_by',
            'updated_by',
            'status',
            'position',
            'created_at',
            'updated_at',
    ],
    ]) ?>

</div>

