<?php

namespace backend\modules\CMS\modules\Block\controllers;

use common\models\c2\statics\CmsBlockType;
use Yii;
use common\models\c2\entity\CmsBlock;
use common\models\c2\search\CmsBlockSearch;
use cza\base\components\controllers\backend\CmsController as Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * AlbumBlockController implements the CRUD actions for CmsBlock model.
 */
class AlbumBlockController extends Controller {

    public $blockType = CmsBlockType::TYPE_ALBUM;
    public $modelClass = 'common\models\c2\entity\CmsBlock';

    public function actions() {
        return ArrayHelper::merge(parent::actions(), [
//            'ueditor'=>[
//                'class' => 'common\widgets\ueditor\UeditorAction',
//                'config'=>[
//                    //上传图片配置
//                    'entity_class' => CmsBlock::className(),
//                    'entity_attribute' => 'content',
//                    'imageUrlPrefix' => '', /* 图片访问路径前缀 */
//                    'imagePathFormat' => "/uploads/{pathHash:CmsBlock}/{id}/{CachingPath}/{hash}", /* 上传保存路径,可以自定义保存路径和文件名格式 */
//                ]
//            ],
                    'editColumn' => [// identifier for your editable action
                        'class' => \backend\components\actions\EditableColumnAction::className(), // action class name
                        'modelClass' => $this->modelClass, // the update model class
                    ],
                    'images-sort' => [
                        'class' => \cza\base\components\actions\common\AttachmentSortAction::className(),
                        'attachementClass' => \common\models\c2\entity\EntityAttachmentImage::className(),
                    ],
                    'images-delete' => [
                        'class' => \cza\base\components\actions\common\AttachmentDeleteAction::className(),
                        'attachementClass' => \common\models\c2\entity\EntityAttachmentImage::className(),
                    ],
                    'images-upload' => [
                        'class' => \cza\base\components\actions\common\AttachmentUploadAction::className(),
                        'attachementClass' => \common\models\c2\entity\EntityAttachmentImage::className(),
                        'entityClass' => CmsBlock::className(),
                        'entityAttribute' => 'album',
//                        'onComplete' => function ($filename, $params) {
//                            Yii::info($filename);
//                            Yii::info($params);
//                        }
                    ],
                    'params-images-upload' => [
                        'class' => \cza\base\components\actions\common\AttachmentUploadAction::className(),
                        'attachementClass' => \common\models\c2\entity\EntityAttachmentImage::className(),
                        'entityClass' => CmsBlock::className(),
                        'entityAttribute' => 'params-album',
//                        }
                    ],
        ]);
    }

    /**
     * Lists all CmsBlock models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new CmsBlockSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, [CmsBlockType::TYPE_ALBUM, CmsBlockType::TYPE_ALBUM_LAYOUT]);

        return $this->render('/album-block/index', [
                    'model' => $this->retrieveModel(),
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CmsBlock model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('/album-block/view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * create/update a CmsBlock model.
     * fit to pajax call
     * @return mixed
     */
    public function actionEdit($id = null) {
        $model = $this->retrieveModel($id);
        $model->type = $this->blockType;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash($model->getMessageName(), [Yii::t('app.c2', 'Saved successful.')]);
            } else {
                Yii::$app->session->setFlash($model->getMessageName(), $model->errors);
            }
        }
        return (Yii::$app->request->isAjax) ? $this->renderAjax('edit', ['model' => $model,]) : $this->render('edit', ['model' => $model,]);
    }

    /**
     * Finds the CmsBlock model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CmsBlock the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = CmsBlock::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
