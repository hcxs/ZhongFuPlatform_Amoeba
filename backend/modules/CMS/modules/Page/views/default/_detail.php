<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


?>
<div class="cms-page-detail">

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'id',
            'type',
            'seo_code',
            'title',
            'breadcrumb',
            'meta_title',
            'meta_keywords:ntext',
            'meta_description:ntext',
            'content_heading:ntext',
            'content:ntext',
            'layout',
            'custom_theme',
            'access_role',
            'views_count',
            'comment_count',
            'is_homepage',
            'is_released',
            'is_draft',
            'released_at',
            'created_by',
            'updated_by',
            'status',
            'position',
            'created_at',
            'updated_at',
    ],
    ]) ?>

</div>

