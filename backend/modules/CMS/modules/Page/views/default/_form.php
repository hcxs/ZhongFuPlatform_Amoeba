<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use cza\base\widgets\ui\adminlte2\InfoBox;
use cza\base\models\statics\EntityModelStatus;
use yii\helpers\Url;

$regularLangName = \Yii::$app->czaHelper->getRegularLangName();
$messageName = $model->getMessageName();
?>

<?php
$form = ActiveForm::begin([
    'action' => ['edit', 'id' => $model->id],
    'options' => [
        'id' => $model->getBaseFormName(),
        'data-pjax' => false,
    ]]);
?>

    <div class="<?= $model->getPrefixName('form') ?>">
        <?php if (Yii::$app->session->hasFlash($messageName)): ?>
            <?php
            if (!$model->hasErrors()) {
                echo InfoBox::widget([
                    'withWrapper' => false,
                    'messages' => Yii::$app->session->getFlash($messageName),
                ]);
            } else {
                echo InfoBox::widget([
                    'defaultMessageType' => InfoBox::TYPE_WARNING,
                    'messages' => Yii::$app->session->getFlash($messageName),
                ]);
            }
            ?>
        <?php endif; ?>

        <div class="well">
            <?php
            echo Html::beginTag('div', ['class' => 'box-footer']);
            echo Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('app.c2', 'Save'), ['type' => 'button', 'class' => 'btn btn-primary pull-right']);
            if (!$model->isNewRecord) {
                echo Html::a(Yii::t('app.c2', 'Formally Browse'), $model->getUrl(FRONTEND_BASE_URL), [
                    'target' => '_blank',
                    'class' => 'btn btn-primary pull-right',
                ]);
                echo Html::a(Yii::t('app.c2', 'Preview'), $model->getPreviewUrl(FRONTEND_BASE_URL), [
                    'target' => '_blank',
                    'class' => 'btn btn-primary pull-right',
                ]);
            }
            echo Html::a('<i class="fa fa-arrow-left"></i> ' . Yii::t('app.c2', 'Go Back'), ['index'], ['data-pjax' => '0', 'class' => 'btn btn-default pull-right', 'title' => Yii::t('app.c2', 'Go Back'),]);
            echo Html::endTag('div');
            echo '<br>';
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 2,
                'attributes' => [
                    //         'type' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => []],
                    'seo_code' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('seo_code')]],
                    //                'layout' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => \common\models\c2\statics\CmsPageLayout::getHashMap('id', 'label')],
                    'layout' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('layout')]],
                    'title' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('title')]],
                    'breadcrumb' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('breadcrumb')]],
                ]
            ]);

            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 1,
                'attributes' => [
                    'meta_description' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => $model->getAttributeLabel('meta_description')]],
                    //                'summary' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\vova07\imperavi\Widget', 'options' => [
                    //                        'settings' => [
                    //                            'minHeight' => 100,
                    //                            'buttonSource' => true,
                    //                            'lang' => $regularLangName,
                    //                            'plugins' => [
                    //                                'fontsize',
                    //                                'fontfamily',
                    //                                'fontcolor',
                    //                                'table',
                    //                                'textdirection',
                    //                                'fullscreen',
                    //                            ],
                    //                        ]
                    //                    ],],
                ],
            ]);

            echo '引用其他内容块标签， 如：<br/>{{internal:SEO_CODE}} SEO_CODE CmsPage表的SEO_CODE字段，根据该字段获取对应文章 <br/>
        {{block:CODE}}   CODE 内容块的编码，根据code 获取指定内容块的content，并插入文章<br/>
        {{album_block:CODE}}  CODE 相册的编码，根据code 获取指定相册块的轮播图集，并插入文章<br/>
        {{widget:WidgetPath|params:{p1=xx&p2=xx}}}，挂件Widget调用方式 <br/>';

            echo Form::widget([
                'model' => $model,
                'form' => $form,
                //            'columns' => 2,
                'attributes' => [
                    'content' => ['type' => Form::INPUT_WIDGET,
                        'widgetClass' => 'common\widgets\ueditor\Ueditor',
                        'options' => [
                            'class' => 'col-sm-6 pull-left',
                        ],
                    ],
                ],
            ]);

            // echo Form::widget([
            //     'model' => $model,
            //     'form' => $form,
            //     //            'columns' => 2,
            //     'attributes' => [
            //         'content' => [
            //             'type' => Form::INPUT_WIDGET,
            //             'widgetClass' => '\vova07\imperavi\Widget',
            //             'options' => [
            //                 'settings' => [
            //                     'lang' => $regularLangName,
            //                     'minHeight' => 450,
            //                     'imageUpload' => Url::to(['image-upload', 'attr' => 'content']),
            //                     'imageManagerJson' => Url::to(['image-list', 'attr' => 'content']),
            //                     'fileUpload' => Url::to(['file-upload', 'attr' => 'content']),
            //                     'fileManagerJson' => Url::to(['file-list', 'attr' => 'content']),
            //                     'buttonSource' => true,
            //                     'replaceDivs' => false, // important, tell editor do not change source codes
            //                     'cleanOnEnter' => false,
            //                     'replaceTags' => false,
            //                     'autoparse' => false,
            //                     'autoparseStart' => false,
            //                     'autoparsePaste' => false,
            //                     'styles' => false,
            //                     'markup' => 'div',
            //                     'pasteKeepStyle' => ['div', 'p', 'td'],
            //                     'plugins' => [
            //                         //                                'inlinestyle',
            //                         'fontsize',
            //                         'fontfamily',
            //                         'fontcolor',
            //                         'table',
            //                         'textdirection',
            //                         'video',
            //                         //                                'clips',
            //                         'filemanager',
            //                         'imagemanager',
            //                         'fullscreen',
            //                     ],
            //                 ]
            //             ],],
            //         //                'content' => ['type' => Form::INPUT_WIDGET,
            //         //                    'widgetClass' => 'common\widgets\ueditor\Ueditor',
            //         //                    'options' => [
            //         //                        'class' => 'col-sm-6 pull-left',
            //         //                    ],
            //         //                ],
            //     ],
            // ]);

            echo Form::widget([
                'model' => $model,
                'form' => $form,
                //            'columns' => 2,
                'attributes' => [
                    //                'is_draft' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\checkbox\CheckboxX', 'options' => [
                    //                        'pluginOptions' => ['threeState' => false],
                    //                    ],],
                    'is_released' => ['type' => Form::INPUT_WIDGET, 'hint' => Yii::t('app.c2', "Publish to access"), 'widgetClass' => '\kartik\checkbox\CheckboxX', 'options' => [
                        'pluginOptions' => ['threeState' => false],
                    ],],
                    'released_at' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\widgets\DateTimePicker', 'options' => [
                        'options' => ['placeholder' => Yii::t('app.c2', 'Date Time...')], 'pluginOptions' => ['format' => 'yyyy-mm-dd hh:ii:ss', 'autoclose' => true],
                    ],],
                    'status' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => EntityModelStatus::getHashMap('id', 'label')],
                    //                'position' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\touchspin\TouchSpin', 'options' => [
                    //                        'pluginOptions' => [
                    //                            'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>',
                    //                            'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>',
                    //                        ],
                    //                    ],],
                ],
            ]);
            echo Html::beginTag('div', ['class' => 'box-footer']);
            echo Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('app.c2', 'Save'), ['type' => 'button', 'class' => 'btn btn-primary pull-right']);
            // if (!$model->isNewRecord) {
            //     echo Html::a(Yii::t('app.c2', 'Formally Browse'), $model->getUrl(ESHOP_BASE_URL), [
            //         'target' => '_blank',
            //         'class' => 'btn btn-primary pull-right',
            //     ]);
            //     echo Html::a(Yii::t('app.c2', 'Preview'), $model->getPreviewUrl(ESHOP_BASE_URL), [
            //         'target' => '_blank',
            //         'class' => 'btn btn-primary pull-right',
            //     ]);
            // }
            echo Html::a('<i class="fa fa-arrow-left"></i> ' . Yii::t('app.c2', 'Go Back'), ['index'], ['data-pjax' => '0', 'class' => 'btn btn-default pull-right', 'title' => Yii::t('app.c2', 'Go Back'),]);
            echo Html::endTag('div');
            ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>
<?php
$js = "";
$js .= "
$('.redactor-editor a').on('click',function(e){
   e.preventDefault();
})

";
$this->registerJs($js);
?>