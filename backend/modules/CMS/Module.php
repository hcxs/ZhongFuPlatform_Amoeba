<?php

namespace backend\modules\CMS;

/**
 * cms module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\CMS\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules = [
            'page' => [
                'class' => 'backend\modules\CMS\modules\Page\Module',
            ],
            'block' => [
                'class' => 'backend\modules\CMS\modules\Block\Module',
            ],
        ];

        // custom initialization code goes here
    }
}
