<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


?>
<div class="amoeba-form-item-detail">

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'id',
            'amoeba_id',
            'amoeba_form_id',
            'seo_code',
            'user_id',
            'label',
            'state',
            'status',
            'position',
            'created_at',
            'updated_at',
    ],
    ]) ?>

</div>

