<?php

namespace backend\modules\Business\modules\AmoebaFormItem\controllers;

use common\models\c2\entity\Amoeba;
use cza\base\models\statics\ResponseDatum;
use Yii;
use common\models\c2\entity\AmoebaFormItem;
use common\models\c2\search\AmoebaFormItemSearch;

use cza\base\components\controllers\backend\ModelController as Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for AmoebaFormItem model.
 */
class DefaultController extends Controller
{
    public $modelClass = 'common\models\c2\entity\AmoebaFormItem';

    public function actions()
    {
        return \yii\helpers\ArrayHelper::merge(parent::actions(), [
            'search-user' => [
                'class' => '\cza\base\components\actions\common\OptionsListAction',
                'modelClass' => \common\models\c2\entity\FeUser::className(),
                'listMethod' => 'getOptionsListCallable',
                'keyAttribute' => 'id',
                'valueAttribute' => 'username',
                'queryAttribute' => 'username',
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ]);
    }
    
    /**
     * Lists all AmoebaFormItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AmoebaFormItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'model' => $this->retrieveModel(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AmoebaFormItem model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * create/update a AmoebaFormItem model.
     * fit to pajax call
     * @return mixed
     */
    public function actionEdit($id = null) 
    {
        $model = $this->retrieveModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash($model->getMessageName(), [Yii::t('app.c2', 'Saved successful.')]);
            } else {
                Yii::$app->session->setFlash($model->getMessageName(), $model->errors);
            }
        }
        
        return (Yii::$app->request->isAjax) ? $this->renderAjax('edit', [ 'model' => $model,]) : $this->render('edit', [ 'model' => $model,]);
    }
    
    /**
     * Finds the AmoebaFormItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AmoebaFormItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AmoebaFormItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionEditWithChart($id = null, $amoeba_id = null)
    {
        $amoebaModel = Amoeba::findOne($amoeba_id);
        $model = new AmoebaFormItem();
        $model->loadDefaultValues();
        $model->amoeba_id = $amoebaModel->id;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash($model->getMessageName(), [Yii::t('app.c2', 'Saved successful.')]);
            } else {
                Yii::$app->session->setFlash($model->getMessageName(), $model->errors);
            }
        }


        return (Yii::$app->request->isAjax) ? $this->renderAjax('edit_with_chart', [
            'model' => $model, 'amoebaModel' => $amoebaModel]) : $this->render('edit_with_chart', [
            'model' => $model, 'amoebaModel' => $amoebaModel]);
    }

    public function actionInitAmoebaForm()
    {
        $params = Yii::$app->request->post();
        if ($model = Amoeba::findOne($params['id'])) {
            if ($model->initAmoebaForm()) {
                $responseData = ResponseDatum::getSuccessDatum(['message' => Yii::t('cza', 'Operation completed successfully!')], $params['id']);
            } else {
                $responseData = ResponseDatum::getErrorDatum(['message' => Yii::t('cza', 'Error: operation can not finish!!')], $params['id']);
            }
        } else {
            $responseData = ResponseDatum::getErrorDatum(['message' => Yii::t('cza', 'Error: operation can not finish!!')], $params['id']);
        }
        return $this->asJson($responseData);
    }

}
