<?php

namespace backend\modules\Business\modules\AmoebaFormItem;

/**
 * amoeba-form-item module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\Business\modules\AmoebaFormItem\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
