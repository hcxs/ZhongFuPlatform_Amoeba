<?php

namespace backend\modules\Business\modules\AmoebaBranch;

/**
 * amoeba-branch module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\Business\modules\AmoebaBranch\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
