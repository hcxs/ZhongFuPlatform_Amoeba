<?php

namespace backend\modules\Business\modules\AmoebaBranch\controllers;

use backend\models\c2\entity\BranchAmoeba;
use common\models\c2\entity\Amoeba;
use common\models\c2\entity\AmoebaFormItem;
use common\models\c2\entity\FeUser;
use cza\base\models\statics\ResponseDatum;
use Yii;
use cza\base\components\controllers\backend\ModelController as Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `amoeba-branch` module
 */
class DefaultController extends Controller
{
    public $modelClass = 'common\models\c2\entity\AmoebaFormItem';

    public function actions()
    {
        return \yii\helpers\ArrayHelper::merge(parent::actions(), [
            'search-user' => [
                'class' => '\cza\base\components\actions\common\OptionsListAction',
                'modelClass' => \common\models\c2\entity\FeUser::className(),
                'listMethod' => 'getOptionsListCallable',
                'keyAttribute' => 'id',
                'valueAttribute' => 'username',
                'queryAttribute' => 'username',
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ]);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Finds the AmoebaFormItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AmoebaFormItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AmoebaFormItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAmoebaGroupWithChart($amoeba_id = null)
    {
        $amoebaModel = BranchAmoeba::findOne($amoeba_id);

        $model = new BranchAmoeba();
        $model->amoeba_id = $amoeba_id;
        $model->loadDefaultValues();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash($model->getMessageName(), [Yii::t('app.c2', 'Saved successful.')]);
            } else {
                Yii::$app->session->setFlash($model->getMessageName(), $model->errors);
            }
        }
        return (Yii::$app->request->isAjax) ? $this->renderAjax('edit_with_chart', [
            'model' => $model, 'amoebaModel' => $amoebaModel]) : $this->render('edit_with_chart', [
            'model' => $model, 'amoebaModel' => $amoebaModel]);
    }

    public function actionBranchAmoebaDelete($id)
    {
        $model = BranchAmoeba::findOne($id);
        if ($model->delete()) {
            $responseData = ResponseDatum::getSuccessDatum(['message' => Yii::t('app.c2', 'Operation completed successfully!')], $id);
        } else {
            $responseData = ResponseDatum::getErrorDatum(['message' => Yii::t('app.c2', 'Error: operation can not finish!!')], $id);
        }
        return $this->asJson($responseData);
    }

    public function actionAssignMemberWithChart($amoeba_id = null)
    {
        $amoebaModel = Amoeba::findOne($amoeba_id);

        $model = new AmoebaFormItem();
        $model->loadDefaultValues();
        $model->amoeba_id = $amoebaModel->id;

//        print_r();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash($model->getMessageName(), [Yii::t('app.c2', 'Saved successful.')]);
            } else {
                Yii::$app->session->setFlash($model->getMessageName(), $model->errors);
            }
        }
//
        return (Yii::$app->request->isAjax) ? $this->renderAjax('assign_with_chart', [
            'model' => $model, 'amoebaModel' => $amoebaModel]) : $this->render('assign_with_chart', [
            'model' => $model, 'amoebaModel' => $amoebaModel]);
    }

    public function actionInitAmoebaForm()
    {
        $params = Yii::$app->request->post();
        if ($model = Amoeba::findOne($params['id'])) {
            if ($model->initAmoebaForm()) {
                $responseData = ResponseDatum::getSuccessDatum(['message' => Yii::t('cza', 'Operation completed successfully!')], $params['id']);
            } else {
                $responseData = ResponseDatum::getErrorDatum(['message' => Yii::t('cza', 'Error: operation can not finish!!')], $params['id']);
            }
        } else {
            $responseData = ResponseDatum::getErrorDatum(['message' => Yii::t('cza', 'Error: operation can not finish!!')], $params['id']);
        }
        return $this->asJson($responseData);
    }

    public function actionMemberKpi($user_id = null, $amoeba_id = null)
    {
        $this->layout = '/main-block';
        $user = FeUser::findOne($user_id);
        return $this->render('member_grade', ['user' => $user, 'amoeba_id' => $amoeba_id]);
    }

}
