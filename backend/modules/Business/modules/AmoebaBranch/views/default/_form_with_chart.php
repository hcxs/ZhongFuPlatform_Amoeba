<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/19 0019
 * Time: 下午 15:58
 */

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use cza\base\widgets\ui\adminlte2\InfoBox;
use cza\base\models\statics\EntityModelStatus;

$regularLangName = \Yii::$app->czaHelper->getRegularLangName();
$messageName = $model->getMessageName();
$this->title = $model->label . ' ' . $amoebaModel->label;

\backend\assets\ChartAsset::register($this);
?>
<div id="chart-container"></div>

<?php
$form = ActiveForm::begin([
    'action' => ['amoeba-group-with-chart', 'id' => $model->id, 'amoeba_id' => $model->amoeba_id],
    'options' => [
        'id' => $model->getBaseFormName(),
        'data-pjax' => true,
    ]]);
?>

<div class="<?= $model->getPrefixName('form') ?>
">
    <?php if (Yii::$app->session->hasFlash($messageName)): ?>
        <?php if (!$model->hasErrors()) {
            echo InfoBox::widget([
                'withWrapper' => false,
                'messages' => Yii::$app->session->getFlash($messageName),
            ]);
        } else {
            echo InfoBox::widget([
                'defaultMessageType' => InfoBox::TYPE_WARNING,
                'messages' => Yii::$app->session->getFlash($messageName),
            ]);
        }
        ?>
    <?php endif; ?>

    <div class="well">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [
                'type' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => [
                        'readonly' => true,
                        'value' => \common\models\c2\statics\AmoebaType::TYPE_BRANCH,
                    ]
                ],
                // 'attributeset_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('attributeset_id')]],
                'label' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('label')]],
                'name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('name')]],
                'province_id' => [
                    'type' => Form::INPUT_DROPDOWN_LIST,
                    'items' => ['0' => Yii::t('app.c2', 'Select Options ...')] + \common\models\c2\entity\RegionProvince::getHashMap('id', 'label'),
                    'options' => ['id' => 'province-id']
                ],
                'city_id' => [
                    'type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\widgets\DepDrop', 'options' => [
                        'data' => empty($model->province_id) ? [] : \common\models\c2\entity\RegionProvince::findOne(['id' => $model->province_id])->getCityHashMap(),
                        'options' => [
                            'id' => 'city-id'
                        ],
                        'pluginOptions' => [
                            'depends' => ['province-id'],
                            'placeholder' => Yii::t('app.c2', 'Select Options ...'),
                            'url' => \yii\helpers\Url::toRoute(['citys'])
                        ],
                    ],
                ],
                'district_id' => [
                    'type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\widgets\DepDrop', 'options' => [
                        'data' => empty($model->city_id) ? [] : \common\models\c2\entity\RegionCity::findOne(['id' => $model->city_id])->getDistrictHashMap(),
                        'pluginOptions' => [
                            'depends' => ['city-id', 'province-id'],
                            'placeholder' => Yii::t('app.c2', 'Select options ..'),
                            'url' => \yii\helpers\Url::toRoute(['districts'])
                        ],
                    ],
                ],
                'code' => ['type' => Form::INPUT_TEXT, 'options' => ['readonly' => true, 'placeholder' => $model->getAttributeLabel('code')]],
                'seo_code' => ['type' => Form::INPUT_TEXT, 'options' => ['readonly' => true, 'placeholder' => $model->getAttributeLabel('seo_code')]],
                // 'geo_longitude' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('geo_longitude')]],
                // 'geo_latitude' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('geo_latitude')]],
                // 'geo_marker_color' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('geo_marker_color')]],
                // 'state' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\checkbox\CheckboxX', 'options' => [
                //     'pluginOptions' => ['threeState' => false],
                // ],],
                'parent_id' => [
                    'type' => Form::INPUT_TEXT,
                    'label' => Yii::t('app.c2', 'Parent ID'),
                    'options' => [
                        'id' => 'selected_id',
                        'readonly' => true,
                    ],
                ],
                'amoeba_id' => [
                    'type' => Form::INPUT_TEXT,
                    'label' => Yii::t('app.c2', 'Amoeba'),
                    'options' => [
                        'readonly' => true,
                    ],
                ],
                'status' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => EntityModelStatus::getHashMap('id', 'label')],
                'position' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\touchspin\TouchSpin', 'options' => [
                    'pluginOptions' => [
                        'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>',
                        'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>',
                    ],
                ],],
            ]
        ]);
        echo Html::beginTag('div', ['class' => 'box-footer']);
        echo Html::a('<i class="fa fa-arrow-left"></i> ' . Yii::t('app.c2', 'Go Back'), ['index'], ['data-pjax' => '0', 'class' => 'btn btn-default', 'title' => Yii::t('app.c2', 'Go Back'),]);
        echo Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('app.c2', 'Save'), ['type' => 'button', 'class' => 'btn btn-primary']);
        echo Html::button('<i class="fa fa-close"></i> ' . Yii::t('app.c2', 'Delete'), ['type' => 'button', 'class' => 'btn btn-danger', 'id' => 'btn-delete-node']);
        echo Html::button('<i class="fa fa-meetup"></i> ' . Yii::t('app.c2', 'Assign Member'), ['type' => 'button', 'class' => 'btn btn-success', 'id' => 'btn-assign-member']);
        echo Html::button('<i class="fa fa-eye"></i> ' . Yii::t('app.c2', 'Amoeba Form Item'), ['type' => 'button', 'class' => 'btn btn-info', 'id' => 'btn-form-member']);
        echo Html::endTag('div');
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>


<script type="text/javascript">
    // JQuery.notConfit();
    $(function ($) {

        var datascource = <?= $amoebaModel->getAmoebaGroupJson() ?>

        var nodeTemplate = function (data) {
            return `<div class="title" data-id="${data.id}" data-type="${data.type}" data-parent-id="${data.parent_id}">
                        ${data.name}
                    </div>`;
        };

        var oc = $('#chart-container').orgchart({
            'data': datascource,
            'chartClass': 'edit-state',
            'exportButton': true,
            'exportFilename': 'SportsChart',
            'parentNodeSymbol': 'fa-th-large',
            'pan': true,
            'zoom': true,
            'nodeTemplate': nodeTemplate
        });

        oc.$chartContainer.on('click', '.node', function () {
            var $this = $(this);
            var selectedId = $this.find('.title').attr('data-id');
            $('#selected_id').val(selectedId).data('node', $this);
        });

        oc.$chartContainer.on('click', '.orgchart', function (event) {
            if (!$(event.target).closest('.node').length) {
                $('#selected_id').val('');
            }
        });

        $('#btn-delete-node').on('click', function () {
            if (confirm('你确定要删除吗？删除节点后该岗位下面人员将会清空！')) {
                var $node = $('#selected_id').data('node');
                if (!$node) {
                    alert('请先选择节点');
                    return;
                }
                if ($node[0] === $('.orgchart').find('.node:first')[0]) {
                    alert('不能删除根节点!');
                    return;
                }
                var id = $('#selected_id').val();
                $.get("<?= \yii\helpers\Url::toRoute('branch-amoeba-delete') ?>", {'id': id}, function (result) {
                    if (result._meta.result === '<?= \cza\base\models\statics\OperationResult::SUCCESS ?>') {
                        oc.removeNodes($node);
                        $('#selected_id').val('').data('node', null);
                    } else {
                        alert(result._meta.message);
                    }
                })
            }
        });

        $('#btn-assign-member').on('click', function (e) {
            var $node = $('#selected_id').data('node');
            if (!$node) {
                alert('请先选择节点');
                return;
            }
            if ($node[0] === $('.orgchart').find('.node:first')[0]) {
                alert('不能分配根节点!');
                return;
            }
            var id = $('#selected_id').val();
            var href = '/business/amoeba-branch/default/assign-member-with-chart'
                + '?amoeba_id=' + id;
            window.open(href, '_blank');
        });

        $('#btn-form-member').on('click', function (e) {
            var $node = $('#selected_id').data('node');
            if (!$node) {
                alert('请先选择节点');
                return;
            }
            if ($node[0] === $('.orgchart').find('.node:first')[0]) {
                alert('不能分配根节点!');
                return;
            }
            var id = $('#selected_id').val();
            var href = '/business/amoeba-form-item/default/edit-with-chart'
                + '?amoeba_id=' + id;
            window.open(href, '_blank');
        });

    });
</script>