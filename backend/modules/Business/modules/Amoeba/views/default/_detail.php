<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


?>
<div class="amoeba-detail">

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'id',
            'type',
            'attributeset_id',
            'province_id',
            'city_id',
            'district_id',
            'code',
            'label',
            'seo_code',
            'geo_longitude',
            'geo_latitude',
            'geo_marker_color',
            'created_by',
            'updated_by',
            'state',
            'status',
            'position',
            'created_at',
            'updated_at',
    ],
    ]) ?>

</div>

