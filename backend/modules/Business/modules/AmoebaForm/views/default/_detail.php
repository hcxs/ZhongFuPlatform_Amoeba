<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


?>
<div class="amoeba-form-detail">

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'id',
            'amoeba_id',
            'type',
            'name',
            'label',
            'parent_id',
            'status',
            'position',
            'created_at',
            'updated_at',
    ],
    ]) ?>

</div>

