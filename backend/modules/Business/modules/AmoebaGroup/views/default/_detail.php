<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


?>
<div class="amoeba-group-detail">

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'id',
            'amoeba_id',
            'children_amoeba_id',
            'parent_amoeba_id',
            'type',
            'state',
            'status',
            'position',
            'created_at',
            'updated_at',
    ],
    ]) ?>

</div>

