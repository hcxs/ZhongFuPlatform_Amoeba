<?php

namespace backend\modules\Business\modules\AmoebaGroup;

/**
 * amoeba-group module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\Business\modules\AmoebaGroup\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
