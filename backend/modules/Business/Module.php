<?php

namespace backend\modules\Business;

/**
 * business module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\Business\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules = [
            'amoeba' => [
                'class' => 'backend\modules\Business\modules\Amoeba\Module',
            ],
            'amoeba-form' => [
                'class' => 'backend\modules\Business\modules\AmoebaForm\Module',
            ],
            'amoeba-form-item' => [
                'class' => 'backend\modules\Business\modules\AmoebaFormItem\Module',
            ],
            'member' => [
                'class' => 'backend\modules\Business\modules\Member\Module',
            ],
            'amoeba-group' => [
                'class' => 'backend\modules\Business\modules\AmoebaGroup\Module',
            ],
            'amoeba-branch' => [
                'class' => 'backend\modules\Business\modules\AmoebaBranch\Module',
            ],


        ];
        // custom initialization code goes here
    }
}
