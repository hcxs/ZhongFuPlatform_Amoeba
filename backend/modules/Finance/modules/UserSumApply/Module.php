<?php

namespace backend\modules\Finance\modules\UserSumApply;

/**
 * user-sum-apply module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\Finance\modules\UserSumApply\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
