<?php

namespace backend\modules\Finance\modules\UserGrade\controllers;

use backend\models\c2\entity\CheckUserGrade;
use common\models\c2\entity\Amoeba;
use common\models\c2\statics\UserGradeState;
use cza\base\models\statics\ResponseDatum;
use Yii;
use common\models\c2\entity\UserGrade;
use common\models\c2\search\UserGradeSearch;

use cza\base\components\controllers\backend\ModelController as Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for UserGrade model.
 */
class DefaultController extends Controller
{
    public $modelClass = 'common\models\c2\entity\UserGrade';
    
    /**
     * Lists all UserGrade models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserGradeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'model' => $this->retrieveModel(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserGrade model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * create/update a UserGrade model.
     * fit to pajax call
     * @return mixed
     */
    public function actionEdit($id = null) 
    {
        $model = $this->retrieveModel($id);
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash($model->getMessageName(), [Yii::t('app.c2', 'Saved successful.')]);
            } else {
                Yii::$app->session->setFlash($model->getMessageName(), $model->errors);
            }
        }
        return (Yii::$app->request->isAjax) ? $this->renderAjax('edit', [ 'model' => $model]) : $this->render('edit', [ 'model' => $model,]);
    }
    
    /**
     * Finds the UserGrade model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return UserGrade the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserGrade::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCheckMemberWithChart($id = null)
    {
        $user = Yii::$app->user->identity;
        $model = CheckUserGrade::findOne($id);
        $model->check_by = $user->id;
        $model->checker_name = $user->fullname;
        $amoebaModel = Amoeba::findOne($model->amoeba_id);



        if ($model->load(Yii::$app->request->post())) {
            if ($model->state != UserGradeState::STATE_INIT) {
                $model->addError('state', Yii::t('app.c2', 'Member was pass.'));
                Yii::$app->session->setFlash($model->getMessageName(), $model->errors);
            } else {
                if ($model->save()) {
                    Yii::$app->session->setFlash($model->getMessageName(), [Yii::t('app.c2', 'Saved successful.')]);
                } else {
                    Yii::$app->session->setFlash($model->getMessageName(), $model->errors);
                }
            }
        }


        return (Yii::$app->request->isAjax) ? $this->renderAjax('edit_with_chart', [
            'model' => $model, 'amoebaModel' => $amoebaModel]) : $this->render('edit_with_chart', [
            'model' => $model, 'amoebaModel' => $amoebaModel]);
    }

    public function actionAssignProfitWithChart($id = null) {
        $model = $this->retrieveModel($id);

        if ($model->state != UserGradeState::STATE_PASS) {
            return false;
        }

        $amoebaModel = Amoeba::findOne($model->amoeba_id);

        return (Yii::$app->request->isAjax) ? $this->renderAjax('edit_with_chart', [
            'model' => $model, 'amoebaModel' => $amoebaModel]) : $this->render('edit_with_chart', [
            'model' => $model, 'amoebaModel' => $amoebaModel]);
    }

    public function actionCommitAssignment($id = null)
    {
        try {
            $model = $this->retrieveModel($id);
            if ($model) {
                if ($model->commitAssignment()) {
                    $responseData = ResponseDatum::getSuccessDatum(['message' => Yii::t('cza', 'Operation completed successfully!')], $id);
                }
            } else {
                $responseData = ResponseDatum::getErrorDatum(['message' => Yii::t('cza', 'Error: operation can not finish!')], $id);
            }
        } catch (\Exception $ex) {
            $responseData = ResponseDatum::getErrorDatum(['message' => $ex->getMessage()], $id);
        }

        return $this->asJson($responseData);
    }

}
