<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


?>
<div class="user-grade-detail">

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'id',
            'amoeba_id',
            'join_user_id',
            'invite_user_id',
            'amoeba_form_item_id',
            'c1_id',
            'dues',
            'type',
            'state',
            'status',
            'position',
            'created_at',
            'updated_at',
    ],
    ]) ?>

</div>

