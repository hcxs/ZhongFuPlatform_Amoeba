<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\c2\search\UserGradeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-grade-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'amoeba_id') ?>

    <?= $form->field($model, 'join_user_id') ?>

    <?= $form->field($model, 'invite_user_id') ?>

    <?= $form->field($model, 'amoeba_form_item_id') ?>

    <?php // echo $form->field($model, 'c1_id') ?>

    <?php // echo $form->field($model, 'dues') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'position') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app.c2', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app.c2', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
