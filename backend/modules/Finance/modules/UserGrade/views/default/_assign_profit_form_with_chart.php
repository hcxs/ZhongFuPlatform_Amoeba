<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use cza\base\widgets\ui\adminlte2\InfoBox;
use cza\base\models\statics\EntityModelStatus;

$regularLangName = \Yii::$app->czaHelper->getRegularLangName();
$messageName = $model->getMessageName();
\backend\assets\ChartAsset::register($this);

$this->title = $amoebaModel->label . '-' . Yii::t('app.c2', 'Assign Profit');
?>

<div id="chart-container"></div>

<?php
$form = ActiveForm::begin([
    'action' => ['check-member-with-chart', 'id' => $model->id,],
    'options' => [
        'id' => $model->getBaseFormName(),
        'data-pjax' => true,
    ]]);
?>

<div class="<?= $model->getPrefixName('form') ?>
">
    <?php if (Yii::$app->session->hasFlash($messageName)): ?>
        <?php if (!$model->hasErrors()) {
            echo InfoBox::widget([
                'withWrapper' => false,
                'messages' => Yii::$app->session->getFlash($messageName),
            ]);
        } else {
            echo InfoBox::widget([
                'defaultMessageType' => InfoBox::TYPE_WARNING,
                'messages' => Yii::$app->session->getFlash($messageName),
            ]);
        }
        ?>
    <?php endif; ?>

    <div class="well">
        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 2,
            'attributes' => [
                'amoeba_form_id' => [
                    'type' => Form::INPUT_TEXT,
                    'label' => Yii::t('app.c2', 'Amoeba Form'),
                    'options' => [
                        'id' => 'amoeba_form_id',
                        'readonly' => true,
                        'placeholder' => $model->getAttributeLabel('amoeba_form_item_id')
                    ]
                ],
                'dues' => [
                    'type' => Form::INPUT_TEXT,
                    'options' => [
                        'readonly' => true,
                        'placeholder' => $model->getAttributeLabel('dues')
                    ]
                ],
                'amoeba_name' => [
                    'type' => Form::INPUT_TEXT,
                    'label' => Yii::t('app.c2', 'Amoeba Name'),
                    'options' => [
                        'readonly' => true,
                        'value' => $model->amoeba->name,
                        'placeholder' => $model->getAttributeLabel('amoeba_id')
                    ]
                ],
                'join_username' => [
                    'type' => Form::INPUT_TEXT,
                    'label' => Yii::t('app.c2', 'Join Username'),
                    'options' => [
                        'readonly' => true,
                        'value' => $model->joinUser->username,
                        'placeholder' => $model->getAttributeLabel('join_user_id')
                    ]
                ],
                'invite_username' => [
                    'type' => Form::INPUT_TEXT,
                    'label' => Yii::t('app.c2', 'Invite Username'),
                    'options' => [
                        'readonly' => true,
                        'value' => $model->inviteUser->username,
                        'placeholder' => $model->getAttributeLabel('invite_user_id')
                    ]
                ],
                'checker_name' => [
                    'type' => Form::INPUT_TEXT,
                    'label' => Yii::t('app.c2', 'Checker Name'),
                    'options' => [
                        'readonly' => true,
                        'value' => $model->checker->profile->fullname,
                    ]
                ],
                // 'is_pass' => [
                //     'type' => Form::INPUT_WIDGET,
                //     'label' => Yii::t('app.c2', 'Is Pass'),
                //     'widgetClass' => '\kartik\checkbox\CheckboxX',
                //     'options' => [
                //         'pluginOptions' => [
                //             'threeState' => false],
                //     ],
                // ],
                // 'type' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => []],
                // 'state' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\checkbox\CheckboxX', 'options' => [
                //     'pluginOptions' => ['threeState' => false],
                // ],],
                // 'status' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => EntityModelStatus::getHashMap('id', 'label')],
                'position' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\touchspin\TouchSpin', 'options' => [
                    'pluginOptions' => [
                        'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>',
                        'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>',
                    ],
                ],],
                'amoeba_id' => [
                    'type' => Form::INPUT_HIDDEN,
                    'options' => [
                        'placeholder' => $model->getAttributeLabel('amoeba_id')
                    ]
                ],
                'join_user_id' => [
                    'type' => Form::INPUT_HIDDEN,
                    'options' => [
                        'placeholder' => $model->getAttributeLabel('join_user_id')
                    ]
                ],
                'invite_user_id' => [
                    'type' => Form::INPUT_HIDDEN,
                    'options' => [
                        'placeholder' => $model->getAttributeLabel('invite_user_id')
                    ]
                ],
                'check_by' => [
                    'type' => Form::INPUT_HIDDEN,
                    'label' => Yii::t('app.c2', 'Amoeba Name'),
                    'options' => [
                        'placeholder' => $model->getAttributeLabel('c1_id')
                    ]
                ],
            ]
        ]);
        echo Html::beginTag('div', ['class' => 'box-footer']);
        // echo Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('app.c2', 'Save'), ['type' => 'button', 'class' => 'btn btn-primary pull-right']);
        echo Html::button('<i class="fa fa-eye"></i> ' . Yii::t('app.c2', 'View Assigned'), ['type' => 'button', 'class' => 'btn btn-info', 'id' => 'assigned']);
        echo Html::a('<i class="fa fa-arrow-left"></i> ' . Yii::t('app.c2', 'Go Back'), ['index'], ['data-pjax' => '0', 'class' => 'btn btn-default pull-right', 'title' => Yii::t('app.c2', 'Go Back'),]);
        echo Html::endTag('div');
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?php

\yii\bootstrap\Modal::begin([
    'id' => 'edit-modal',
    'size' => 'modal-lg',
]);

\yii\bootstrap\Modal::end();

?>


<script type="text/javascript">
    // JQuery.notConfit();
    $(function ($) {

        let datascource = <?= $amoebaModel->getAmoebaFormJson(['withMember' => true]) ?>

        // var nodeTemplate = function (data) {
        //     return `<div class="title" data-id="${data.id}" data-type="${data.type}" data-parent-id="${data.parent_id}">${data.name}</div>`;
        // };

            let nodeTemplate = function (data) {
            let tag = `<div class="title" data-id="${data.id}" data-type="${data.type}">${data.name}</div>`;
            tag += `<div class="warpper">`;
            if (data.memberList) {
                data.memberList.map(function (item) {
                    tag += `<a href="javascript:;" data-id="${item.user.id}" data-pjax='0'
                                class="assign glyphicon glyphicon-adjust">${item.user.username}</a>`
                })
            }
            tag += '</div>';
            return tag;
        };

        let oc = $('#chart-container').orgchart({
            'data': datascource,
            'chartClass': 'edit-state',
            'exportButton': true,
            'exportFilename': 'SportsChart',
            'parentNodeSymbol': 'fa-th-large',
            'pan': true,
            'zoom': true,
            'nodeTemplate': nodeTemplate
        });

        oc.$chartContainer.on('click', '.node', function () {
            // var $this = $(this);
            // var selectedId = $this.find('.title').attr('data-id');
            // $('#amoeba_form_id').val(selectedId).data('node', $this);
        });

        oc.$chartContainer.on('click', '.orgchart', function (event) {
            // if (!$(event.target).closest('.node').length) {
            //     $('#amoeba_form_id').val('');
            // }
        });

        $('.assign').on('click', function (e) {
            let user_id = jQuery(e.currentTarget).attr('data-id');
            let href = '/finance/user-profit-item/default/edit'
                + '?user_id=' + user_id
                + '&grade_id=' + <?= $model->id ?>;
            jQuery('#edit-modal').modal('show').find('.modal-content').html('....').load(href);
            // location.href = href;
        });

        $('#assigned').on('click', function (e) {
            let href = '/finance/user-profit-item/default/index'
                + '?UserProfitItemSearch[grade_id]=' + <?= $model->id ?>;
            // jQuery('#edit-modal').modal('show').find('.modal-content').html('....').load(href);
            location.href = href;
        });

    });
</script>