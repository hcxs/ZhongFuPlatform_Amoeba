<?php

use cza\base\widgets\ui\common\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use cza\base\models\statics\EntityModelStatus;
use cza\base\models\statics\OperationEvent;

/* @var $this yii\web\View */
/* @var $searchModel common\models\c2\search\UserGradeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app.c2', 'User Grades');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="well user-grade-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'id' => $model->getPrefixName('grid'),
        'pjax' => true,
        'hover' => true,
        'showPageSummary' => true,
        'panel' => ['type' => GridView::TYPE_PRIMARY, 'heading' => Yii::t('app.c2', 'Items')],
        'toolbar' => [
            [
                'content' =>
//                    Html::a('<i class="glyphicon glyphicon-plus"></i>', ['edit'], [
//                        'class' => 'btn btn-success',
//                        'title' => Yii::t('app.c2', 'Add'),
//                        'data-pjax' => '0',
//                    ]) . ' ' .
                    Html::button('<i class="glyphicon glyphicon-remove"></i>', [
                        'class' => 'btn btn-danger',
                        'title' => Yii::t('app.c2', 'Delete Selected Items'),
                        'onClick' => "jQuery(this).trigger('" . OperationEvent::DELETE_BY_IDS . "', {url:'" . Url::toRoute('multiple-delete') . "'});",
                    ]) . ' ' .
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', Url::current(), [
                        'class' => 'btn btn-default',
                        'title' => Yii::t('app.c2', 'Reset Grid')
                    ]),
            ],
            '{export}',
            '{toggleData}',
        ],
        'exportConfig' => [],
        'columns' => [
            ['class' => 'kartik\grid\CheckboxColumn'],
            ['class' => 'kartik\grid\SerialColumn'],
            [
                'class' => 'kartik\grid\ExpandRowColumn',
                'expandIcon' => '<span class="fa fa-plus-square-o"></span>',
                'collapseIcon' => '<span class="fa fa-minus-square-o"></span>',
                'detailUrl' => Url::toRoute(['detail']),
                'value' => function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
            ],
            'id',
            // 'amoeba_id',
            [
                'attribute' => 'amoeba_id',
                'value' => function ($model) {
                    return $model->amoeba->name;
                }
            ],
            // 'join_user_id',
            [
                'attribute' => 'join_user_id',
                'value' => function ($model) {
//             print_r($model);
                    return $model->joinUser->username;
                }
            ],
            // 'invite_user_id',
            [
                'attribute' => 'invite_user_id',
                'value' => function ($model) {
                    return $model->inviteUser->username;
                }
            ],
            // 'amoeba_form_item_id',
            // 'check_by',
            [
                'attribute' => 'check_by',
                'value' => function ($model) {
                    return $model->checker ? $model->checker->profile->fullname : '';
                }
            ],
            // 'dues',
            // 'type',
            // 'state',
            [
                'attribute' => 'state',
                'value' => function ($model) {
                    return \common\models\c2\statics\UserGradeState::getLabel($model->state);
                },
                'filter' => \common\models\c2\statics\UserGradeState::getHashMap('id', 'label')
            ],
            // 'status',
            // 'position',
            'created_at',
            // 'updated_at',
            // [
            //     'attribute' => 'status',
            //     'class' => '\kartik\grid\EditableColumn',
            //     'editableOptions' => [
            //         'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
            //         'formOptions' => ['action' => Url::toRoute('editColumn')],
            //         'data' => EntityModelStatus::getHashMap('id', 'label'),
            //         'displayValueConfig' => EntityModelStatus::getHashMap('id', 'label'),
            //     ],
            //     'filter' => EntityModelStatus::getHashMap('id', 'label'),
            //     'value' => function ($model) {
            //         return $model->getStatusLabel();
            //     }
            // ],
            [
                'class' => '\kartik\grid\ActionColumn',
                'template' => '{check_member} {assign_profit} {commit_assignment}',
                'visibleButtons' => [
                    'check_member' => function($model) {
                        return $model->isStateInit();
                    },
                    'assign_profit' => function($model) {
                        return $model->isStatePass();
                    },
                    'commit_assignment' => function($model) {
                        return $model->isStatePass();
                    },
                ],
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['edit', 'id' => $model->id], [
                            'title' => Yii::t('app', 'Info'),
                            'data-pjax' => '0',
                        ]);
                    },
                    'check_member' => function ($url, $model, $key) {
                        return Html::a(Yii::t('app.c2', 'Check Member'), [
                            'check-member-with-chart', 'id' => $model->id
                        ], [
                            'title' => Yii::t('app.c2', 'Check Member'),
                            'data-pjax' => '0',
                            'class' => 'btn btn-info btn-xs',
                        ]);
                    },
                    'assign_profit' => function ($url, $model, $key) {
                        return Html::a(Yii::t('app.c2', 'Assign Profit'), [
                            'assign-profit-with-chart', 'id' => $model->id
                        ], [
                            'title' => Yii::t('app.c2', 'Assign Profit'),
                            'data-pjax' => '0',
                            'class' => 'btn btn-success btn-xs',
                        ]);
                    },
                    'commit_assignment' => function ($url, $model, $key) {
                        return Html::a(Yii::t('app.c2', 'Commit Assignment'), [
                            'commit-assignment', 'id' => $model->id
                        ], [
                            'title' => Yii::t('app.c2', 'Commit Assignment'),
                            'data-pjax' => '0',
                            'class' => 'btn btn-danger btn-xs commit',
                        ]);
                    },
                ]
            ],

        ],
    ]); ?>

</div>
<?php
$js = "";

$js .= "jQuery(document).off('click', 'a.commit').on('click', 'a.commit', function(e) {
                e.preventDefault();
                var lib = window['krajeeDialog'];
                var url = jQuery(e.currentTarget).attr('href');
                lib.confirm('" . Yii::t('app.c2', 'Are you sure commit assignment?') . "', function (result) {
                    if (!result) {
                        return;
                    }
                    
                    jQuery.ajax({
                            url: url,
                            success: function(data) {
                                var lifetime = 6500;
                                if(data._meta.result == '" . cza\base\models\statics\OperationResult::SUCCESS . "'){
                                    jQuery('#{$model->getPrefixName('grid')}').trigger('" . OperationEvent::REFRESH . "');
                                }
                                else{
                                  lifetime = 16500;
                                }
                                jQuery.msgGrowl ({
                                        type: data._meta.type, 
                                        title: '" . Yii::t('cza', 'Tips') . "',
                                        text: data._meta.message,
                                        position: 'top-center',
                                        lifetime: lifetime,
                                });
                            },
                            error :function(data){alert(data._meta.message);}
                    });
                });
            });";


$js .= "$.fn.modal.Constructor.prototype.enforceFocus = function(){};";   // fix select2 widget input-bug in popup

$this->registerJs($js);
?>