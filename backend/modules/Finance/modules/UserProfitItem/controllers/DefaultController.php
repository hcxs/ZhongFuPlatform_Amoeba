<?php

namespace backend\modules\Finance\modules\UserProfitItem\controllers;

use common\models\c2\entity\UserGrade;
use common\models\c2\statics\UserGradeState;
use Yii;
use common\models\c2\entity\UserProfitItem;
use common\models\c2\search\UserProfitItemSearch;

use cza\base\components\controllers\backend\ModelController as Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for UserProfitItem model.
 */
class DefaultController extends Controller
{
    public $modelClass = 'common\models\c2\entity\UserProfitItem';
    
    /**
     * Lists all UserProfitItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        // $this->layout = '/main-block';
        $searchModel = new UserProfitItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index_modal', [
            'model' => $this->retrieveModel(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserProfitItem model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * create/update a UserProfitItem model.
     * fit to pajax call
     * @return mixed
     */
    public function actionEdit($id = null, $user_id = null, $grade_id)
    {
        $grade = UserGrade::findOne($grade_id);

        if (is_null($grade) && $grade->state != UserGradeState::STATE_PASS) {
            return false;
        }

        $model = $this->retrieveModel($id);
        $model->user_id = $user_id;
        $model->grade_id = $grade->id;
        $model->amoeba_id = $grade->amoeba_id;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash($model->getMessageName(), [Yii::t('app.c2', 'Saved successful.')]);
            } else {
                Yii::$app->session->setFlash($model->getMessageName(), $model->errors);
            }
        }
        
        return (Yii::$app->request->isAjax) ? $this->renderAjax('edit', [ 'model' => $model,]) : $this->render('edit', [ 'model' => $model,]);
    }
    
    /**
     * Finds the UserProfitItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserProfitItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserProfitItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
