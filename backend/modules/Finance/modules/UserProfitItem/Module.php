<?php

namespace backend\modules\Finance\modules\UserProfitItem;

/**
 * user-profit-item module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\Finance\modules\UserProfitItem\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
