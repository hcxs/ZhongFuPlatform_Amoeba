<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


?>
<div class="user-profit-item-detail">

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'id',
            'type',
            'grade_id',
            'amoeba_id',
            'user_id',
            'income',
            'state',
            'status',
            'created_at',
            'updated_at',
    ],
    ]) ?>

</div>

