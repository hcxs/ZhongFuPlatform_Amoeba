<?php

namespace backend\modules\Finance;

/**
 * finance module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\Finance\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules = [
            'user-grade' => [
                'class' => 'backend\modules\Finance\modules\UserGrade\Module',
            ],
            'user-profit-item' => [
                'class' => 'backend\modules\Finance\modules\UserProfitItem\Module',
            ],
            'user-sum-apply' => [
                'class' => 'backend\modules\Finance\modules\UserSumApply\Module',
            ],
        ];
        // custom initialization code goes here
    }
}
