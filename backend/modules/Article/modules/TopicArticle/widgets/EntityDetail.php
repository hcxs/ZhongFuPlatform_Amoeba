<?php

namespace backend\modules\Article\modules\TopicArticle\widgets;

use Yii;
use cza\base\widgets\ui\common\part\EntityDetail as DetailWidget;

/**
 * Entity Detail Widget
 *
 * @author Ben Bi <ben@cciza.com>
 * @link http://www.cciza.com/
 * @copyright 2014-2016 CCIZA Software LLC
 * @license
 */
class EntityDetail extends DetailWidget
{
    public $withTranslationTabs = false;

    public $withProfileTab = false;
    public $withArticleFileTab = false;

    public function getTabItems() {
        $items = [];

        if ($this->withArticleFileTab) {
            $items[] = $this->getArticleFileTab();
        }

        if ($this->withBaseInfoTab) {
            $items[] = [
                'label' => Yii::t('app.c2', 'Base Information'),
                'content' => $this->controller->renderPartial('_form', ['model' => $this->model,]),
                'active' => true,
            ];
        }

        $items[] = [
            'label' => '<i class="fa fa-th"></i> ' . $this->tabTitle,
            'onlyLabel' => true,
            'headerOptions' => [
                'class' => 'pull-left header',
            ],
        ];

        return $items;
    }

    public function getArticleFileTab() {
        if (!isset($this->_tabs['FILE_TAB'])) {
            if (!$this->model->isNewRecord) {
                $this->_tabs['FILE_TAB'] = [
                    'label' => Yii::t('app.c2', 'Article File'),
                    'content' => $this->controller->renderPartial('_files_manager_form', ['model' => $this->model]),
                    'enable' => true,
                ];
            } else {
                $this->_tabs['FILE_TAB'] = [
                    'label' => Yii::t('app.c2', 'Article File'),
                    'content' => "",
                    'enable' => false,
                ];
            }
        }

        return $this->_tabs['FILE_TAB'];
    }

}