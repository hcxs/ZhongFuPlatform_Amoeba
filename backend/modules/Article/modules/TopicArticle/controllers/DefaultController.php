<?php

namespace backend\modules\Article\modules\TopicArticle\controllers;

use Yii;
use common\models\c2\entity\TopicArticle;
use common\models\c2\search\TopicArticleSearch;

use cza\base\components\controllers\backend\ModelController as Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DefaultController implements the CRUD actions for TopicArticle model.
 */
class DefaultController extends Controller
{
    // public $modelClass = 'common\models\c2\entity\TopicArticle';
    public $modelClass = 'backend\models\c2\entity\TopicArticle';

    public function actions() {
        return [
            'ueditor' => [
                'class' => 'common\widgets\ueditor\UeditorAction',
                'config' => [
                    //上传图片配置
                    'entity_class' => \common\models\c2\entity\TopicArticle::className(),
                    'entity_attribute' => 'content',
                    'imageUrlPrefix' => '', /* 图片访问路径前缀 */
                    'imagePathFormat' => "/uploads/{pathHash:default}/{id}/{CachingPath}/{hash}", /* 上传保存路径,可以自定义保存路径和文件名格式 */
                ]
            ],
            'files-sort' => [
                'class' => \cza\base\components\actions\common\AttachmentSortAction::className(),
                'attachementClass' => \common\models\c2\entity\EntityAttachmentFile::className(),
            ],
            'files-delete' => [
                'class' => \cza\base\components\actions\common\AttachmentDeleteAction::className(),
                'attachementClass' => \common\models\c2\entity\EntityAttachmentFile::className(),
            ],
            'files-upload' => [
                'class' => \cza\base\components\actions\common\AttachmentUploadAction::className(),
                'attachementClass' => \common\models\c2\entity\EntityAttachmentFile::className(),
                'entityClass' => \common\models\c2\entity\TopicArticle::className(),
                'entityAttribute' => 'files',
                //                        'onComplete' => function ($filename, $params) {
                //                            Yii::info($filename);
                //                            Yii::info($params);
                //                        }
            ],
        ];
    }
    
    /**
     * Lists all TopicArticle models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TopicArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'model' => $this->retrieveModel(),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TopicArticle model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * create/update a TopicArticle model.
     * fit to pajax call
     * @return mixed
     */
    public function actionEdit($id = null) 
    {
        $model = $this->retrieveModel($id);
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash($model->getMessageName(), [Yii::t('app.c2', 'Saved successful.')]);
            } else {
                Yii::$app->session->setFlash($model->getMessageName(), $model->errors);
            }
        }
        
        return (Yii::$app->request->isAjax) ? $this->renderAjax('edit', [ 'model' => $model,]) : $this->render('edit', [ 'model' => $model,]);
    }
    
    /**
     * Finds the TopicArticle model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return TopicArticle the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = \backend\models\c2\entity\TopicArticle::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
