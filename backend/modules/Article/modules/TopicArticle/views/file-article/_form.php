<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use cza\base\widgets\ui\adminlte2\InfoBox;
use cza\base\models\statics\EntityModelStatus;

$regularLangName = \Yii::$app->czaHelper->getRegularLangName();
$messageName = $model->getMessageName();

$css = '
.poler-content-block {
    /* margin-top: 25px; */
    height: 525px;
    padding: 57px 10px 0 15px;
    background: url(/images/admins/iphone.png) no-repeat;
    position: relative;
    box-sizing: border-box;
}

.poler-visual-content {
    background-color: #fff;
    content: close-quote;
    width: 226px;
    height: 410px;
    overflow-y: auto;
    overflow-x: hidden;
}
';
$this->registerCss($css);

\yii\bootstrap\Modal::begin([
    'id' => 'preview-modal',
    'header' => '<h4 class="modal-title">效果预览</h4>',
    'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>',
    'size' => \yii\bootstrap\Modal::SIZE_DEFAULT
]);
$js = "$('.modal-body').load('preview');";
$this->registerJs($js);
\yii\bootstrap\Modal::end();
?>

<?php
$form = ActiveForm::begin([
    'action' => ['edit', 'id' => $model->id],
    'options' => [
        'id' => $model->getBaseFormName(),
        'data-pjax' => true,
    ]]);
?>

    <div class="<?= $model->getPrefixName('form') ?>">
        <?php if (Yii::$app->session->hasFlash($messageName)): ?>
            <?php
            if (!$model->hasErrors()) {
                echo InfoBox::widget([
                    'withWrapper' => false,
                    'messages' => Yii::$app->session->getFlash($messageName),
                ]);
            } else {
                echo InfoBox::widget([
                    'defaultMessageType' => InfoBox::TYPE_WARNING,
                    'messages' => Yii::$app->session->getFlash($messageName),
                ]);
            }
            ?>
        <?php endif; ?>

        <div class="well">
            <?php
            // echo Html::beginTag('div', ['class' => 'box-footer']);
            // echo Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('app.c2', 'Save'), ['type' => 'button', 'class' => 'btn btn-primary pull-right']);
            // echo Html::a('<i class="fa fa-arrow-left"></i> ' . Yii::t('app.c2', 'Go Back'), ['index'], ['data-pjax' => '0', 'class' => 'btn btn-default pull-right', 'title' => Yii::t('app.c2', 'Go Back'),]);
            // echo Html::endTag('div');

            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 1,
                'attributes' => [
                    'avatar' => [
                        'type' => Form::INPUT_WIDGET,
                        'label'=>Yii::t('app.c2','Title Picture'),
                        'widgetClass' => '\kartik\widgets\FileInput',
                        'options' => [
                            'options' => [
                                'accept' => 'image/*',
                            ],
                            'pluginOptions' => [
                                'overwriteInitial' => true,
                                'maxFileSize' => 1500,
                                'showClose' => false,
                                'showCaption' => false,
                                'browseLabel' => '',
                                'removeLabel' => '',
                                'browseIcon' => '<i class="glyphicon glyphicon-folder-open"></i>',
                                'removeIcon' => '<i class="glyphicon glyphicon-remove"></i>',
                                'removeTitle' => 'Cancel or reset changes',
                                'elErrorContainer' => '#kv-avatar-errors-1',
                                'msgErrorClass' => 'alert alert-block alert-danger',
                                'defaultPreviewContent' => '<img src="/images/common/default_img.png" alt="' . Yii::t('app.c2', '{s1} avatar', ['s1' => Yii::t('app.c2', 'Topic Article')]) . '" style="width:160px">',
                                'layoutTemplates' => "{main2: '{preview} {browse} {remove}'}",
                                'allowedFileExtensions' => ["jpg", "png", "gif"],
                                'showUpload' => false,
                                'initialPreview' => $model->getInitialPreview('avatar', \cza\base\models\statics\ImageSize::ORGINAL),
                                'initialPreviewConfig' => $model->getInitialPreview('avatar'),
                            ],
                        ],
                    ],
                ]
            ]);
            ?>

            <?php
            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 2,
                'attributes' => [
                    'title' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('title')]],
                    'type' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => \common\models\c2\statics\TopicArticleType::getHashMap('id', 'label')],
                    'meta_title' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('meta_title')]],
                    'meta_description' => ['type' => Form::INPUT_TEXTAREA, 'options' => ['placeholder' => $model->getAttributeLabel('meta_description')]],
                ]
            ]);

            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 1,
                'attributes' => [
                    'topic_ids' => [
                        'type' => Form::INPUT_WIDGET,
                        'widgetClass' => '\kartik\widgets\Select2',
                        'options' => [
                            'language' => Yii::$app->language,
                            'data' => \common\models\c2\entity\Topic::getHashMap('id', 'title'),
                            'pluginOptions' => [
                                'multiple' => true
                            ],
                        ],
                    ]
                ],
            ]);

            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 1,
                'attributes' => [
                    'summary' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\vova07\imperavi\Widget', 'options' => [
                        'settings' => [
                            'minHeight' => 150,
                            'buttonSource' => true,
                            'lang' => $regularLangName,
                            'plugins' => [
                                'fontsize',
                                'fontfamily',
                                'fontcolor',
                                'table',
                                'textdirection',
                                'fullscreen',
                            ],
                        ]
                    ],],
                ]
            ]);

            // echo Form::widget([
            //     'model' => $model,
            //     'form' => $form,
            //     //            'columns' => 2,
            //     'attributes' => [
            //         'content' => ['type' => Form::INPUT_WIDGET,
            //             'widgetClass' => 'common\widgets\ueditor\Ueditor',
            //             'options' => [
            //                 'class' => 'col-sm-6 pull-left',
            //             ],
            //         ],
            //     ],
            // ]);

            echo Form::widget([
                'model' => $model,
                'form' => $form,
                'columns' => 2,
                'attributes' => [
                    //                'author_id' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('author_id')]],
                    'author_name' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('author_name')]],
                    //                'views_count' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('views_count')]],
                    //                'comment_count' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('comment_count')]],
                    'released_at' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\widgets\DateTimePicker', 'options' => [
                        'options' => ['placeholder' => Yii::t('app.c2', 'Date Time...')], 'pluginOptions' => ['format' => 'yyyy-mm-dd hh:ii:ss', 'autoclose' => true],
                    ],],
                    'is_released' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\checkbox\CheckboxX', 'options' => [
                        'pluginOptions' => ['threeState' => false],
                    ],],
                    'is_shared' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\checkbox\CheckboxX', 'options' => [
                        'pluginOptions' => ['threeState' => false],
                    ],],
                    'status' => ['type' => Form::INPUT_DROPDOWN_LIST, 'items' => EntityModelStatus::getHashMap('id', 'label')],
                    'position' => ['type' => Form::INPUT_WIDGET, 'widgetClass' => '\kartik\touchspin\TouchSpin', 'options' => [
                        'pluginOptions' => [
                            'buttondown_txt' => '<i class="glyphicon glyphicon-minus-sign"></i>',
                            'buttonup_txt' => '<i class="glyphicon glyphicon-plus-sign"></i>',
                        ],
                    ],],
                ]
            ]);

            echo Html::beginTag('div', ['class' => 'box-footer']);
            echo Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('app.c2', 'Save'), ['type' => 'button', 'class' => 'btn btn-primary pull-right']);
            echo Html::a('<i class="fa fa-arrow-left"></i> ' . Yii::t('app.c2', 'Go Back'), ['index'], ['data-pjax' => '0', 'class' => 'btn btn-default pull-right', 'title' => Yii::t('app.c2', 'Go Back'),]);
            echo Html::endTag('div');
            ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>

<?php
$js = '
$(function(){
	    $(\'#preview-modal\').on(\'show.bs.modal\', function () {
	    if(window.Storage && window.localStorage && window.localStorage instanceof Storage){
                     var str = localStorage.ueditor_preference;
                     if(str.length>2){
                     var split = str.split(\'":"\');
                    var key = split[0];
                    var key = key.replace(\'{"\',"");
                    console.log("解析http后"+key);
                    var contentObj = JSON.parse(str);
                    var content = contentObj[key];
                                    	console.log(content);
                    $(".poler-visual-content").html(content);
                     }else{
                     var content = \'请先编辑内容再查看预览效果\';
                    $(".poler-visual-content").html(content);
                     }
                    
        }else{
                var content = \'请启用浏览器LocalStorage才能查看预览效果\';
                    $(".poler-visual-content").html(content);
        }
	                
        })
	})
';

$jsw = '$(function(){
            console.log("xasdasd");

    setTimeout(function(){
    var content = localStorage.preview_content;
            $(".poler-visual-content").html(content);
            console.log(content);
    },100);
})
';
// $this->registerJs($js);
?>