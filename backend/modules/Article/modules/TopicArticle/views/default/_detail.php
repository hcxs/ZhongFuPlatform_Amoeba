<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


?>
<div class="topic-article-detail">

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'id',
            'type',
            'title',
            'summary:ntext',
            'content:ntext',
            'author_id',
            'author_name',
            'views_count',
            'comment_count',
            'released_at',
            'is_released',
            'is_shared',
            'created_by',
            'updated_by',
            'status',
            'position',
            'created_at',
            'updated_at',
    ],
    ]) ?>

</div>

