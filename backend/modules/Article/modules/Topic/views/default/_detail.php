<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


?>
<div class="topic-detail">

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'id',
            'type',
            'code',
            'title',
            'render_definition:ntext',
            'summary:ntext',
            'content:ntext',
            'views_count',
            'created_by',
            'updated_by',
            'status',
            'position',
            'created_at',
            'updated_at',
    ],
    ]) ?>

</div>

