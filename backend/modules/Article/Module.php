<?php

namespace backend\modules\Article;

/**
 * article module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'backend\modules\Article\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        $this->modules = [
            'topic-article' => [
                'class' => 'backend\modules\Article\modules\TopicArticle\Module',
            ],
            'topic' => [
                'class' => 'backend\modules\Article\modules\Topic\Module',
            ],

        ];

        // custom initialization code goes here
    }
}
