<?php

/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    // common
    'App Console' => '众富创业平台后台管理系统',
    'Welcome to Backend Console' => '欢饮使用后台管理系统',
    'Please use sidebar menu to process...' => '请使用侧边菜单进行操作...',
    'Menu' => '菜单',
    'Dashboard' => '仪表板',
    'Database' => '资料库',
    '{s1} Management' => '{s1}管理',
    'Product' => '产品',
    'User' => '用户',
    'Measure' => '单位',
    'Currency' => '货币',
    'Logistics' => '后勤资料',
    'System' => '系统',
    'Configuration' => '参数',
    'Common Resource' => '公共资源',
    'Attachement Management' => '附件管理',
    'Global Settings' => '全局配置',
    'Security' => '安全',
    'Users & Rbac' => '角色权限',
    'Sign out' => '退出帐号',
    'Config' => '配置',
    'Logs' => '日志',
    'Region' => '地区',
    'Close' => '关闭',
    'Save' => '保存',
    'Go Back' => '返回',
    'Update' => '更新',
    'Create' => '创建',
    'Cancel' => '取消',
    'Created By' => '创建者',
    'Are you sure?' => '你确定执行改动作吗',
    'Init' => '初始化',
    'Finish' => '完成',
    'View' => '查看',
    'Created At' => '创建时间',
    'Updated At' => '更新时间',

    'Business' => '业务',
    'Amoeba' => '',
    'Type' => '类型',
    'Code' => '编号',
    'Name' => '名称',
    'Label' => '标签',
    'Default' => '默认',
    'Branch' => '分支',
    'Province ID' => '省份',
    'City ID' => '市/区',
    'District ID' => '镇/区',
    'Status' => '状态',
    'Active' => '活动',
    'InActive' => '暂停',
    'Amoeba Form' => '组织架构',
    'Amoeba Form Item' => '架构成员',
    'Amoeba Form Type C3' => '高级总监',
    'Amoeba Form Type C2' => '总监',
    'Amoeba Form Type C1' => '经理',
    'Amoeba Form Type B' => '主任',
    'Amoeba Form Type A' => '组长',
    'Amoeba Form Type P' => '会员',
    'Member' => '会员',
    'Fe Users' => '会员列表',
    'Fe User' => '会员',
    'Username' => '用户名称',
    'Invite Username' => '邀请人名称',
    'Last Login Ip' => '最后登录IP',
    'Mobile Number' => '手机号码',
    'Registration Src Type' => '注册来源',
    'Select Options ...' => '请选择...',
    'Last Login At' => '最后登录时间',
    'Registration Ip' => '注册IP',
    'Add' => '添加',
    'Delete' => '删除',
    'Amoeba Group' => '分盘',
    'Parent ID' => '父级ID',
    'Amoeba Branch' => '分盘',
    'Error: operation can not finish!!' => '警告：操作不能完成！',
    'Operation completed successfully!' => '操作成功!',
    'Assign Member' => '分配成员',
    'Parent Amoeba' => '上一级 Amoeba',
    'Finance' => '财务',
    'Join User ID' => '加入会员名称',
    'Invite User ID' => '邀请人名称',
    'Checker Name' => '审核人名称',
    'User Grades' => '用户绩效列表',
    'User Grade' => '用户绩效',
    'Waiting Assign Profit' => '待分配收益',
    'Organization Structure' => '组织架构',
    'Check Member' => '新会员审核',
    'State' => '流程状态',
    'Amoeba Name' => 'Amoeba 名称',
    'Join Username' => '新会员名称',
    'Dues' => '转帐金额',
    'Is Pass' => '是否通过',
    'Member was pass.' => '用户已审核！',
    'Assign Profit' => '分配收益',
    'Pass' => '审核通过',
    'Income' => '收益',
    'View Assigned' => '查看分配',
    'UnConfirm' => '待确认',
    'Commit' => '已确认',
    'Commit Assignment' => '确认分配',
    'Are you sure commit assignment?' => '你确定完成分配收益吗？',
    'Are you sure commit apply?' => '你确定对方提现申请到账吗？',
    'User Sum Apply' => '用户提现申请',
    'User Sum Applies' => '用户提现申请列表',
    'Apply Init' => '待审核',
    'Apply Sum' => '申请金额',
    'Bankcard' => '银行卡',
    'Ali Pay' => '支付宝',
    'Wechat' => '微信转账',
    'Apply Commit' => '确认到账',
    'Apply Turning' => '审核',
    'Transfer Rate' => '手续费',
    'Received Sum' => '转账金额',
    'Bankcard Number' => '银行卡号',
    'Memo' => '备注',
    'Confirmed By' => '审核人',
    'Confirmed At' => '审核日期',
    'Reject' => '拒绝',
    'Turning' => '转账中',
    'GenericBlock' => '通用板块',
    'AlbumBlock' => '相册板块',
    'Page' => '页面',
    'Block' => '板块',
    'Cms AlbumBlocks' => '相册板块列表',
    'Cms AlbumBlock' => '相册板块',
    'Cms Blocks' => '通用板块列表',
    'Cms Block' => '通用板块',
    'Cms AlbumLayoutBlock' => '相册布局',
    'Cms Block Item Management' => '内容块子集管理',
    'Scroll Pic' => '轮播图',
    '{s1} Album' => '{s1}图集',
    'Base Information' => '基本信息',
    'Article' => '文章',
    'Topic Article' => '主题文章',
    'Topic' => '主题',
    'Topics' => '主题列表',
    'Topic Articles' => '主题文章列表',
    'Title' => '标题',
    'Author Name' => '发布人',
    'Is Released' => '是否发布',
    'Is Shared' => '是否能分享',
    'Comment Count' => '评论数量',
    'Released At' => '发布时间',
    'Content' => '内容',
    'Summary' => '简要',
    'Meta Description' => 'Meta简要',
    'Meta Keywords' => 'Meta关键词',
    'Meta Title' => 'Meta标题',
    'Title Picture' => '文章封面',
    'Preview' => '预览',
    'Formally Browse' => '正式访问',
    'File' => '文件',
    'Video' => '视频',
    '' => '',
];

