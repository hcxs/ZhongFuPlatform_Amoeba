/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2019/6/24 星期一 下午 3:58:15                     */
/*==============================================================*/


drop index Index_3 on c2_cms_block;

drop index Index_2 on c2_cms_block;

drop index Index_1 on c2_cms_block;

drop table if exists c2_cms_block;

drop index Index_3 on c2_cms_block_item;

drop index Index_2 on c2_cms_block_item;

drop index Index_1 on c2_cms_block_item;

drop table if exists c2_cms_block_item;

drop index Index_3 on c2_cms_block_item_lang;

drop index Index_2 on c2_cms_block_item_lang;

drop index Index_1 on c2_cms_block_item_lang;

drop table if exists c2_cms_block_item_lang;

drop index Index_3 on c2_cms_block_lang;

drop index Index_2 on c2_cms_block_lang;

drop index Index_1 on c2_cms_block_lang;

drop table if exists c2_cms_block_lang;

drop index Index_5 on c2_cms_page;

drop index Index_4 on c2_cms_page;

drop index Index_3 on c2_cms_page;

drop index Index_2 on c2_cms_page;

drop index Index_1 on c2_cms_page;

drop table if exists c2_cms_page;

drop index Index_5 on c2_cms_page_lang;

drop index Index_4 on c2_cms_page_lang;

drop index Index_3 on c2_cms_page_lang;

drop index Index_2 on c2_cms_page_lang;

drop index Index_1 on c2_cms_page_lang;

drop table if exists c2_cms_page_lang;

drop index Index_1 on c2_cms_page_tag;

drop table if exists c2_cms_page_tag;

drop index Index_2 on c2_cms_page_tag_rs;

drop index Index_1 on c2_cms_page_tag_rs;

drop table if exists c2_cms_page_tag_rs;

drop index Index_4 on c2_topic;

drop index Index_3 on c2_topic;

drop index Index_2 on c2_topic;

drop index Index_1 on c2_topic;

drop table if exists c2_topic;

drop index Index_5 on c2_topic_article;

drop index Index_4 on c2_topic_article;

drop index Index_3 on c2_topic_article;

drop index Index_2 on c2_topic_article;

drop index Index_1 on c2_topic_article;

drop table if exists c2_topic_article;

drop index Index_3 on c2_topic_article_category_rs;

drop index Index_2 on c2_topic_article_category_rs;

drop index Index_1 on c2_topic_article_category_rs;

drop table if exists c2_topic_article_category_rs;

drop index Index_3 on c2_topic_article_comment;

drop index Index_2 on c2_topic_article_comment;

drop index Index_1 on c2_topic_article_comment;

drop table if exists c2_topic_article_comment;

drop index Index_3 on c2_topic_article_lang;

drop index Index_2 on c2_topic_article_lang;

drop index Index_1 on c2_topic_article_lang;

drop table if exists c2_topic_article_lang;

drop index Index_1 on c2_topic_article_rs;

drop table if exists c2_topic_article_rs;

drop index Index_6 on c2_topic_category;

drop index Index_5 on c2_topic_category;

drop index Index_4 on c2_topic_category;

drop index Index_3 on c2_topic_category;

drop index Index_2 on c2_topic_category;

drop index Index_1 on c2_topic_category;

drop table if exists c2_topic_category;

drop index Index_3 on c2_topic_lang;

drop index Index_2 on c2_topic_lang;

drop index Index_1 on c2_topic_lang;

drop table if exists c2_topic_lang;

/*==============================================================*/
/* Table: c2_cms_block                                          */
/*==============================================================*/
create table c2_cms_block
(
   id                   bigint not null auto_increment,
   type                 tinyint default 0,
   code                 varchar(255),
   label              varchar(255) not null,
   url                  varchar(255),
   content              mediumtext,
   created_by           bigint default 0,
   updated_by           bigint default 0,
   status               tinyint default 1,
   position             int default 0,
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on c2_cms_block
(
   code
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create index Index_2 on c2_cms_block
(
   created_by
);

/*==============================================================*/
/* Index: Index_3                                               */
/*==============================================================*/
create index Index_3 on c2_cms_block
(
   type
);

/*==============================================================*/
/* Table: c2_cms_block_item                                     */
/*==============================================================*/
create table c2_cms_block_item
(
   id                   bigint not null auto_increment,
   block_id             bigint default 0,
   type                 tinyint default 0,
   code                 varchar(255),
   label              varchar(255) not null,
   content              text,
   link                 varchar(255),
   status               tinyint default 1,
   position             int default 0,
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on c2_cms_block_item
(
   code
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create index Index_2 on c2_cms_block_item
(
   block_id
);

/*==============================================================*/
/* Index: Index_3                                               */
/*==============================================================*/
create index Index_3 on c2_cms_block_item
(
   block_id,
   type
);

/*==============================================================*/
/* Table: c2_cms_block_item_lang                                */
/*==============================================================*/
create table c2_cms_block_item_lang
(
   id                   bigint not null auto_increment,
   entity_id            bigint default 0,
   language             varchar(255),
   label              varchar(255) not null,
   content              mediumtext,
   status               tinyint default 1,
   position             int default 0,
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on c2_cms_block_item_lang
(
   language
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create index Index_2 on c2_cms_block_item_lang
(
   entity_id
);

/*==============================================================*/
/* Index: Index_3                                               */
/*==============================================================*/
create index Index_3 on c2_cms_block_item_lang
(
   entity_id,
   language
);

/*==============================================================*/
/* Table: c2_cms_block_lang                                     */
/*==============================================================*/
create table c2_cms_block_lang
(
   id                   bigint not null auto_increment,
   entity_id            bigint default 0,
   language             varchar(255),
   label              varchar(255) not null,
   content              mediumtext,
   status               tinyint default 1,
   position             int default 0,
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on c2_cms_block_lang
(
   language
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create index Index_2 on c2_cms_block_lang
(
   entity_id
);

/*==============================================================*/
/* Index: Index_3                                               */
/*==============================================================*/
create index Index_3 on c2_cms_block_lang
(
   entity_id,
   language
);

/*==============================================================*/
/* Table: c2_cms_page                                           */
/*==============================================================*/
create table c2_cms_page
(
   id                   bigint not null auto_increment,
   type                 tinyint default 0,
   seo_code             varchar(255),
   title                varchar(255) not null,
   breadcrumb           varchar(255),
   meta_title           varchar(255),
   meta_keywords        text,
   meta_description     text,
   content_heading      text,
   content              mediumtext,
   layout               varchar(255),
   custom_theme         varchar(255),
   access_role          varchar(255),
   views_count          int default 0,
   comment_count        int default 0,
   is_homepage          tinyint default 0,
   is_released          tinyint default 1,
   is_draft             tinyint default 1,
   released_at          datetime,
   created_by           bigint default 0,
   updated_by           bigint default 0,
   status               tinyint default 1,
   position             int default 0,
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on c2_cms_page
(
   seo_code
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create index Index_2 on c2_cms_page
(
   created_by
);

/*==============================================================*/
/* Index: Index_3                                               */
/*==============================================================*/
create index Index_3 on c2_cms_page
(
   type
);

/*==============================================================*/
/* Index: Index_4                                               */
/*==============================================================*/
create index Index_4 on c2_cms_page
(
   is_released
);

/*==============================================================*/
/* Index: Index_5                                               */
/*==============================================================*/
create index Index_5 on c2_cms_page
(
   is_homepage
);

/*==============================================================*/
/* Table: c2_cms_page_lang                                      */
/*==============================================================*/
create table c2_cms_page_lang
(
   id                   bigint not null auto_increment,
   entity_id            bigint default 0,
   language             varchar(255),
   seo_code             varchar(255),
   title                varchar(255),
   breadcrumb           varchar(255),
   meta_title           varchar(255),
   meta_keywords        text,
   meta_description     text,
   content_heading      text,
   content              mediumtext,
   custom_theme         varchar(255),
   status               tinyint default 1,
   position             int default 0,
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on c2_cms_page_lang
(
   language
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create index Index_2 on c2_cms_page_lang
(
   entity_id
);

/*==============================================================*/
/* Index: Index_3                                               */
/*==============================================================*/
create index Index_3 on c2_cms_page_lang
(
   entity_id,
   language
);

/*==============================================================*/
/* Index: Index_4                                               */
/*==============================================================*/
create index Index_4 on c2_cms_page_lang
(
   seo_code
);

/*==============================================================*/
/* Index: Index_5                                               */
/*==============================================================*/
create index Index_5 on c2_cms_page_lang
(
   title
);

/*==============================================================*/
/* Table: c2_cms_page_tag                                       */
/*==============================================================*/
create table c2_cms_page_tag
(
   id                   bigint not null auto_increment,
   label              varchar(255) not null,
   created_by           bigint default 0,
   updated_by           bigint default 0,
   status               tinyint default 1,
   position             int default 0,
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on c2_cms_page_tag
(
   label
);

/*==============================================================*/
/* Table: c2_cms_page_tag_rs                                    */
/*==============================================================*/
create table c2_cms_page_tag_rs
(
   id                   bigint not null auto_increment,
   owner_id             bigint default 0,
   item_id              bigint default 0,
   status               tinyint default 1,
   position             int default 0,
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on c2_cms_page_tag_rs
(
   owner_id
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create index Index_2 on c2_cms_page_tag_rs
(
   item_id
);

/*==============================================================*/
/* Table: c2_topic                                              */
/*==============================================================*/
create table c2_topic
(
   id                   bigint not null auto_increment,
   type                 tinyint default 0,
   code                 varchar(255),
   title                varchar(255) not null,
   render_definition    text,
   summary              text,
   content              text,
   views_count          int default 0,
   created_by           bigint default 0,
   updated_by           bigint default 0,
   status               tinyint default 1,
   position             int default 0,
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on c2_topic
(
   code
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create index Index_2 on c2_topic
(
   type
);

/*==============================================================*/
/* Index: Index_3                                               */
/*==============================================================*/
create index Index_3 on c2_topic
(
   code
);

/*==============================================================*/
/* Index: Index_4                                               */
/*==============================================================*/
create index Index_4 on c2_topic
(
   title
);

/*==============================================================*/
/* Table: c2_topic_article                                      */
/*==============================================================*/
create table c2_topic_article
(
   id                   bigint not null auto_increment,
   type                 tinyint default 0,
   title                varchar(255),
   meta_title           varchar(255),
   meta_keywords        text,
   meta_description     text,
   summary              text,
   content              text,
   author_id            int default 0,
   author_name          varchar(255),
   views_count          int default 0,
   comment_count        int default 0,
   released_at          datetime,
   is_released          tinyint default 1,
   is_shared            tinyint not null default 1,
   created_by           bigint default 0,
   updated_by           bigint default 0,
   status               tinyint default 1,
   position             int default 0,
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on c2_topic_article
(
   position
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create index Index_2 on c2_topic_article
(
   title
);

/*==============================================================*/
/* Index: Index_3                                               */
/*==============================================================*/
create index Index_3 on c2_topic_article
(
   author_name
);

/*==============================================================*/
/* Index: Index_4                                               */
/*==============================================================*/
create index Index_4 on c2_topic_article
(
   author_name
);

/*==============================================================*/
/* Index: Index_5                                               */
/*==============================================================*/
create index Index_5 on c2_topic_article
(
   author_id
);

/*==============================================================*/
/* Table: c2_topic_article_category_rs                          */
/*==============================================================*/
create table c2_topic_article_category_rs
(
   id                   bigint not null auto_increment,
   category_id          bigint not null default 0,
   article_id           bigint not null default 0,
   status               tinyint default 1,
   position             int default 100,
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on c2_topic_article_category_rs
(
   article_id
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create index Index_2 on c2_topic_article_category_rs
(
   category_id,
   article_id
);

/*==============================================================*/
/* Index: Index_3                                               */
/*==============================================================*/
create index Index_3 on c2_topic_article_category_rs
(
   category_id
);

/*==============================================================*/
/* Table: c2_topic_article_comment                              */
/*==============================================================*/
create table c2_topic_article_comment
(
   id                   bigint not null auto_increment,
   article_id           bigint default 0,
   language             varchar(255),
   title                varchar(255) not null,
   content              text,
   status               tinyint default 1,
   position             int default 0,
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on c2_topic_article_comment
(
   language
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create index Index_2 on c2_topic_article_comment
(
   article_id
);

/*==============================================================*/
/* Index: Index_3                                               */
/*==============================================================*/
create index Index_3 on c2_topic_article_comment
(
   title
);

/*==============================================================*/
/* Table: c2_topic_article_lang                                 */
/*==============================================================*/
create table c2_topic_article_lang
(
   id                   bigint not null auto_increment,
   entity_id            bigint default 0,
   language             varchar(255) default '0',
   title                varchar(255) not null,
   summary              text,
   content              text,
   author_name          varchar(255),
   status               tinyint default 1,
   position             int default 0,
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on c2_topic_article_lang
(
   language
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create index Index_2 on c2_topic_article_lang
(
   entity_id,
   language
);

/*==============================================================*/
/* Index: Index_3                                               */
/*==============================================================*/
create index Index_3 on c2_topic_article_lang
(
   entity_id
);

/*==============================================================*/
/* Table: c2_topic_article_rs                                   */
/*==============================================================*/
create table c2_topic_article_rs
(
   id                   bigint not null auto_increment,
   topic_id             bigint not null default 0,
   article_id           bigint not null default 0,
   status               tinyint default 1,
   position             int default 100,
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on c2_topic_article_rs
(
   topic_id,
   article_id
);

/*==============================================================*/
/* Table: c2_topic_category                                     */
/*==============================================================*/
create table c2_topic_category
(
   id                   bigint not null auto_increment,
   code                 varchar(255),
   label              varchar(255),
   parent_id            bigint default 0,
   `left`                 int default 0,
   `right`                int default 0,
   level                int default 0,
   type                 tinyint default 0,
   topic_id             bigint default 0,
   status               tinyint default 1,
   position             int default 0,
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on c2_topic_category
(
   `left`
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create index Index_2 on c2_topic_category
(
   `right`
);

/*==============================================================*/
/* Index: Index_3                                               */
/*==============================================================*/
create index Index_3 on c2_topic_category
(
   level
);

/*==============================================================*/
/* Index: Index_4                                               */
/*==============================================================*/
create index Index_4 on c2_topic_category
(
   parent_id
);

/*==============================================================*/
/* Index: Index_5                                               */
/*==============================================================*/
create index Index_5 on c2_topic_category
(
   code
);

/*==============================================================*/
/* Index: Index_6                                               */
/*==============================================================*/
create index Index_6 on c2_topic_category
(
   topic_id
);

/*==============================================================*/
/* Table: c2_topic_lang                                         */
/*==============================================================*/
create table c2_topic_lang
(
   id                   bigint not null auto_increment,
   item_id              bigint default 0,
   language             varchar(255),
   title                varchar(255) not null,
   summary              text,
   content              text,
   status               tinyint default 1,
   position             int default 0,
   created_at           datetime,
   updated_at           datetime,
   primary key (id)
);

/*==============================================================*/
/* Index: Index_1                                               */
/*==============================================================*/
create index Index_1 on c2_topic_lang
(
   language
);

/*==============================================================*/
/* Index: Index_2                                               */
/*==============================================================*/
create index Index_2 on c2_topic_lang
(
   item_id,
   language
);

/*==============================================================*/
/* Index: Index_3                                               */
/*==============================================================*/
create index Index_3 on c2_topic_lang
(
   item_id
);

