<?php

namespace frontend\modules\Center\controllers;

use common\models\c2\entity\UserSumApply;
use common\models\c2\search\UserProfitItemSearch;
use common\models\c2\search\UserSumApplySearch;
use Da\QrCode\Exception\InvalidConfigException;
use frontend\components\actions\UserQRCodeAction;
use frontend\controllers\BaseController;
use frontend\models\UserSumApplyForm;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Default controller for the `center` module
 */
class DefaultController extends BaseController
{

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->navActive = 4;
        return $this->render('index');
    }

    public function actionProfit()
    {
        $this->layout = '/empty';
        $user_id = Yii::$app->user->id;
        $searchModel = new UserProfitItemSearch();
        $searchModel->user_id = $user_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('profit', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSumApply()
    {
        $this->layout = '/empty';
        $model = new UserSumApplyForm();
        $model->user_id = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->apply()) {
                return $this->render('sumApplySuccess', [
                    'message' => Yii::t('app.c2', 'Sum Apply Success')]);
            }
        }

        return $this->render('sumApply', ['model' => $model]);
    }

    public function actionSumApplyRecord()
    {
        $this->layout = '/empty';
        $searchModel = new UserSumApplySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('sumApplyRecord', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

}
