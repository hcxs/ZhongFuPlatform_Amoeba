<?php

namespace frontend\modules\Home\controllers;

use common\models\c2\entity\CmsPage;
use frontend\controllers\BaseController;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `home` module
 */
class DefaultController extends BaseController
{

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($seo_code = 'homepage')
    {
        $model = CmsPage::getValidPage($seo_code);
        // if (is_null($model)) {
        //     throw new NotFoundHttpException(Yii::t('app.c2', "Page not found."));
        // }
        $this->navActive = 1;
        return $this->render('index', [
            'model' => $model
        ]);
    }
}
