<?php

namespace frontend\modules\CMS;

use Yii;
//use app_eshop\components\Module as BaseModule;
use yii\base\Module as BaseModule;

/**
 * customer module definition class
 */
class Module extends BaseModule {

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'frontend\modules\CMS\controllers';

    /**
     * @inheritdoc
     */
    public function init() {
        parent::init();

        // custom initialization code goes here
    }
//    public function checkAccess() {
//        if (Yii::$app->wechat->isWechat && !Yii::$app->wechat->isAuthorized()) {
//            return false;
//        } else {
//            return true;
//        }
//    }
}
