<?php

namespace frontend\modules\CMS\controllers;

use frontend\controllers\BaseController;
use Yii;
use app_eshop\components\Controller;
use common\models\c2\search\Shop;
use common\widgets\TencentPoiMarker;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;

/**
 * Default controller for the `customer` module
 */
class DefaultController extends BaseController {


    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }
    
    /**
     * Renders the index view for the module
     * @return string
     */

    public function actionIndex() {
        $this->layout = '/main_user_without_header';
        return $this->render('index');
    }
    

}
