<?php

namespace frontend\modules\Study\controllers;

use common\models\c2\entity\TopicArticle;
use common\models\c2\statics\TopicArticleType;
use cza\base\models\statics\EntityModelStatus;
use frontend\controllers\BaseController;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

/**
 * Default controller for the `study` module
 */
class DefaultController extends BaseController
{

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->navActive = 3;

        $query = TopicArticle::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query->andWhere(['status' => EntityModelStatus::STATUS_ACTIVE,])
                ->orderBy('created_at DESC'),
            'pagination' => [
                'pageSize' => 10,
                'params' => Yii::$app->request->get(),
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionDetail($id)
    {
        $this->layout = '/empty';
        $model = \common\models\c2\entity\TopicArticle::findOne($id);
        $model->updateCounters(['views_count' => 1]);
        if ($model->type == TopicArticleType::TYPE_FILE) {
            $files = $model->pdfAttachments;
            return $this->render('files', ['files' => $files, 'model' => $model]);
        }
        return $this->render('detail', [
            'model' => $model
        ]);
    }
}
