<?php

namespace frontend\modules\Amoeba\controllers;

use common\models\c2\entity\Amoeba;
use common\models\c2\entity\AmoebaFormItem;
use frontend\controllers\BaseController;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `amoeba` module
 */
class DefaultController extends BaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex($id = null)
    {
        $this->layout = '/empty';

        $userId = Yii::$app->user->id;
        $amoebaFormItemModel = AmoebaFormItem::find()
            ->where(['user_id' => $userId, 'amoeba_id' => $id])->one();

        if (is_null($amoebaFormItemModel)) {
            throw new NotFoundHttpException(Yii::t('app.c2', 'Params Error'));
        }

        $model = Amoeba::findOne($id);

        return $this->render('index', [
            'model' => $model
        ]);

    }
}
