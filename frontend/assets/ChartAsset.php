<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class ChartAsset extends AssetBundle
{
//     public $basePath = '@webroot';
//     public $baseUrl = '@web';
    public $css = [

//        'js/slidercaptcha.css',
        'css/font-awesome.min.css',
        'css/jquery.orgchart.css',
        'css/style.css',

    ];

    public $js = [
        'js/html2canvas.min.js',
        'js/jquery.orgchart.js',
//        'js/jquery-1.11.0.min.js',
//        'js/longbow.slidercaptcha.js',



    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];

    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];

    public function init()
    {
        $this->sourcePath = '@app/themes/' . CZA_FRONTEND_THEME . '/assets/org_chart';
        parent::init();
    }
}
