<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/1/24
 * Time: 21:17
 */

use frontend\themes\sunflower\AppAsset;
use yii\helpers\Html;
AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="initial-scale=1, maximum-scale=3, minimum-scale=1, user-scalable=no">
    <!--UC浏览器禁止横屏-->
    <meta name="screen-orientation"content="portrait">
    <!--QQ浏览器禁止横屏-->
    <meta name="x5-orientation" content="portrait">
    <meta http-equiv="pragma" content="no-cache" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php if (isset($this->params['header'])): ?>
    <header>
        <div class="header">
            <?= isset($this->params['back']) ? Html::a('<img src="/images/icon/back.png" />', 'javascript:history.back()', [
                'class' => 'header-back'
            ]) : '' ?>

            <span class="header-title"><?= $this->params['title'] ?></span>
        </div>
    </header>
<?php endif; ?>

<div class="main">
    <?= $content ?>

</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
