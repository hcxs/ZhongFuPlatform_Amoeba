<?php

use yii\helpers\Url;
?>
<!--footer 开始-->
<div class="container-fluid bottom-bar">
    <div class="row">
        <a href="/home" class="col-xs-3 bottom-bar-item">
            <img src="<?= $this->context->navActive == 1 ? '/images/icon/home_active.png' : '/images/icon/home.png' ?>">
            <p class="bottom-bar-item-text <?= $this->context->navActive == 1 ? 'active-text' : '' ?>"><?= Yii::t('app.c2', 'Home') ?></p>
        </a>
        <a href="/amoeba-form/default/list" class="col-xs-3 bottom-bar-item">
            <img src="<?= $this->context->navActive == 2 ? '/images/icon/form_active.png' : '/images/icon/form.png' ?>">
            <p class="bottom-bar-item-text <?= $this->context->navActive == 2 ? 'active-text' : '' ?>"><?= Yii::t('app.c2', 'Amoeba Form') ?></p>
        </a>
        <a href="/study" class="col-xs-3 bottom-bar-item">
            <img src="<?= $this->context->navActive == 3 ? '/images/icon/study_active.png' : '/images/icon/study.png' ?>">
            <p class="bottom-bar-item-text <?= $this->context->navActive == 3 ? 'active-text' : '' ?>"><?= Yii::t('app.c2', 'Study') ?></p>
        </a>
        <a href="/user-center" class="col-xs-3 bottom-bar-item">
            <img src="<?= $this->context->navActive == 4 ? '/images/icon/user_active.png' : '/images/icon/user.png' ?>">
            <p class="bottom-bar-item-text <?= $this->context->navActive == 4 ? 'active-text' : '' ?>"><?= Yii::t('app.c2', 'User Center') ?></p>
        </a>
    </div>
</div>
<!--footer end-->