<?php

$this->title = Yii::t('app.c2', 'Home');
// $this->params['back'] = true;
$this->params['header'] = true;
$this->params['title'] = Yii::t('app.c2', 'Home');

?>

<style>
    img {
        width: 100%;
    }
</style>
<?php

if (!is_null($model)) {
    $model->renderContent();
    echo $model->content;
}

?>
