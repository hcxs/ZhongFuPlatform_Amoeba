<?php

use yii\widgets\ListView;

$this->title = Yii::t('app.c2', 'Study');
// $this->params['back'] = true;
$this->params['header'] = true;
$this->params['title'] = Yii::t('app.c2', 'Study');

?>

    <style>
        .article-row {
            background-color: white;
            margin: 10px 0 0 0;
        }
    </style>
<?=

ListView::widget([
    'dataProvider' => $dataProvider,
    // 'layout' => "{items}\n{pager}",
    'itemOptions' => ['class' => 'item row article-row'],
    'options' => ['class' => 'list-view container-fluid'],
    'itemView' => '_topic_article_item',
    'summary' => '',
    'pager' => [
        'class' => \common\widgets\Y2sp\ScrollPager::className(),
        // 'next' => '.next a',
        // 'triggerOffset' => 20,
        // 'spinnerTemplate' => '<div class="ias-spinner" style="text-align: center;"><img src="/images/home/loading.png"/></div>',
        'triggerTemplate' => '<div class="ias-spinner btn btn-block btn-link" style="text-align: center;">加载更多</div>',
    ]
]);
?>