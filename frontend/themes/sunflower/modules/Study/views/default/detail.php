<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/24 0024
 * Time: 下午 14:39
 */
$this->title = $model->title;
$this->params['back'] = true;
$this->params['header'] = true;
$this->params['title'] = Yii::t('app.c2', 'Article Detail');
?>
<style>
    article {
        /*background: white;*/
    }

    h4 {
        margin: 0;
        padding: 10px;
        background: white;
    }

    .article-info {
        font-size: 12px;
        color: gray;
        padding: 10px;
        margin-bottom: 10px;
        background: white;
    }

    .article-detail {
        padding: 10px;
        background: white;
    }

    .article-detail img {
        width: 100%;
    }

    .article-summary {
        padding-top: 10px;
        margin-top: 10px;
        color: gray;
        font-style: italic;
        border-bottom: 1px solid #eee;
        border-top: 1px solid #eee;
        background: white;
    }

    iframe {
        width: 100%;
    }
</style>

<article>
    <h4><?= $model->title; ?></h4>
    <div class="article-info">
        <div>
            <?= Yii::t('app.c2', 'Released At:{s1}', ['s1' => date('Y-m-d', strtotime($model->released_at))]) ?>
            <?= Yii::t('app.c2', 'Author:{s1}', ['s1' => $model->author_name]) ?>
        </div>

        <div class="article-summary">
            <?= $model->summary ?>
        </div>
    </div>

    <div class="article-detail">
        <?= $model->content; ?>
    </div>

</article>