<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/24 0024
 * Time: 下午 13:49
 */

?>

<style>
    .article-content {
        margin-bottom: 10px;
        margin-left: 10px;
    }

    .article-info {
        font-size: 12px;
        color: gray;
    }

    .media-object {
        width: 64px;
    }

    a:link {
        color: black;
        text-decoration: none;
    }
</style>

<div class="media article-content">
    <div class="media-left">
        <a href="<?= $model->getDetailUrl() ?>">
            <img class="media-object" src="<?= $model->getThumbnailUrl() ?>" alt="cover">
        </a>
    </div>
    <div class="media-body">
        <a href="<?= $model->getDetailUrl() ?>">
            <h4 class="media-heading">
                <?php foreach ($model->topics as $topic): ?>
                    <span class="label label-info"><?= $topic->title ?></span>
                <?php endforeach;?>
                <?= $model->title ?>
            </h4>
            <p class="article-info">
                <?= Yii::t('app.c2', 'Released At:{s1}', ['s1' => date('Y-m-d', strtotime($model->released_at))]) ?>
                <?= Yii::t('app.c2', 'Author:{s1}', ['s1' => $model->author_name]) ?>
            </p>
        </a>
    </div>
</div>
