<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/27 0027
 * Time: 上午 10:54
 */
$this->title = $model->title;
$this->params['back'] = true;
$this->params['header'] = true;
$this->params['title'] = Yii::t('app.c2', 'Study');
?>
<style>
    .filename {
        margin: 0
    }
</style>


<?php foreach ($files as $file): ?>

    <object data="<?= $file->getFileUrl() ?>" type="application/pdf" width="100%" height="750px">
        <embed src="<?= $file->getFileUrl() ?>" type="application/pdf">
        <div style="text-align: center">
            <p><?= Yii::t('app.c2', 'This browser does not support PDFs. Please download the PDF to view it: ') ?></p>
            <a href="<?= $file->getFileUrl() ?>" target="_blank" class="btn btn-info">
                <span class="glyphicon glyphicon-download-alt"></span>
                <p class="filename"><?= $file->name ?></p>
            </a>
        </div>
        </embed>
    </object>

<?php endforeach; ?>
