<?php

/* @var $this yii\web\View */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('app.c2', 'Amoeba Form Select');
// $this->params['back'] = true;
$this->params['header'] = true;
$this->params['title'] = Yii::t('app.c2', 'Amoeba Form Select');

?>
<style>
    .box {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-evenly;
        align-content: space-evenly;
        width: 100%;
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .box-item {
        /** width: 46%; */

        text-align: left;
    }
</style>
<div class="container-fluid">

    <?php if (count($models) == 0): ?>
        <div class="alert alert-warning" role="alert"><?= Yii::t('app.c2', 'Pls wait for check.') ?></div>
    <?php endif; ?>


    <div class="box">
        <?php foreach ($models as $model): ?>
            <?= Html::a(
                '<p>' . Yii::t('app.c2', 'Amoeba:') . $model->amoeba->name . '</p>'
                . '<p>' . $model->amoebaForm->name . '</p>'
                . '<p>' . Yii::t('app.c2', 'Code:') . $model->seo_code . '</p>',
                ['index', 'id' => $model->seo_code], ['class' => 'box-item btn btn-default']) ?>


        <?php endforeach; ?>
    </div>

</div>
