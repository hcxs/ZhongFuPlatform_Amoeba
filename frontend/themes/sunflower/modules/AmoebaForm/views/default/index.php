<?php

use yii\bootstrap\Modal;
use yii\helpers\Url;

$this->title = Yii::t('app.c2', 'Amoeba Form');
// $this->params['back'] = true;
$this->params['header'] = true;
// $this->params['title'] = Yii::t('app.c2', 'Amoeba Form');
$this->params['title'] = $model->name;
\frontend\assets\ChartAsset::register($this);
?>
<style>

    /*#content { position: absolute }*/
    /*#chart-container {*/
    /*position: absolute*/
    /*}*/
</style>

<div id="print" class="container-fluid">
    <div id="chart-container"></div>


    <div class="block">
        <div class="block-content">
            <?= \yii\helpers\Html::a(Yii::t('app.c2', 'My KPI'), [
                '/amoeba', 'id' => $model->id
            ], ['class' => 'col-xs-10']) ?>
            <div class="col-xs-2" style="text-align: right"><img class="block-right" src="/images/icon/right.png"></div>
        </div>
    </div>

    <div class="block" style="margin-top: 1px;">
        <div class="block-content">
            <?= \yii\helpers\Html::a(Yii::t('app.c2', 'Invite Member'), 'javascript:;', [
                'class' => 'col-xs-10 qr',
                'data-pjax' => '0',
                'data-value' => Url::toRoute(['invite-code', 'amoeba_id' => $model->code]),
            ]); ?>
            <div class="col-xs-2" style="text-align: right"><img class="block-right" src="/images/icon/right.png"></div>
        </div>
    </div>

</div>

<?php
Modal::begin([
    'id' => 'common-modal',
    'header' => '<h4 class="modal-title"></h4>',
    'footer' => '<a href="#" class="btn btn-primary" data-dismiss="modal">Close</a>',
]);
Modal::end();

$js = <<<JS
$(document).off('click', 'a.qr').on('click','a.qr',function(e, data){
    // e.preventDefault();
    jQuery('#common-modal').modal('show')
    .find('.modal-content')
    .html('<img class="qr-content">')
    .css("text-align", "center");
    $(".qr-content").attr('src', jQuery(e.currentTarget).attr('data-value'));
});
JS;
$this->registerJs($js);
?>


<script type="text/javascript">
    // JQuery.notConfit();
    $(function ($) {

        var datascource = <?= $model->getAmoebaFormJson(['withMember' => true]) ?>

        var nodeTemplate = function (data) {

            var tag = `<div class="title" data-id="${data.id}" data-type="${data.type}">${data.name}</div>`;
            tag += `<div class="warpper">`;
            if (data.memberList) {
                data.memberList.map(function (item) {
                    tag += `<p>${item.user.username}</p>`
                    if (item.user.invite_username != null) {
                        tag += `<p style="color:#199475">推荐人:${item.user.invite_username}</p>`
                    }
                })
            }
            tag += '</div>';
            return tag;
        };

        var oc = $('#chart-container').orgchart({
            'data': datascource,
            'chartClass': 'edit-state',
            'exportButton': false,
            'exportFilename': 'SportsChart',
            'parentNodeSymbol': 'fa-th-large',
            'pan': true,
            'zoom': true,
            'nodeTemplate': nodeTemplate
        });

        // oc.$chartContainer.on('click', '.node', function () {
        //
        // });

        // oc.$chartContainer.on('click', '.orgchart', function (event) {
        //     if (!$(event.target).closest('.node').length) {
        //         $('#selected-node').val('');
        //     }
        // });

        // const evt = "onorientationchange" in window ? "orientationchange" : "resize";
        // window.addEventListener(evt, function () {
        //     const width = document.documentElement.clientWidth;
        //     const height = document.documentElement.clientHeight;
        //     const contentDOM = document.getElementById('chart-container');
        //     console.log('width: ' + width + ' height: ' + height)
        //     if (width > height) { // 横屏
        //         contentDOM.style.width = width + 'px';
        //         contentDOM.style.height = height + 'px';
        //         contentDOM.style.top = '0px';
        //         contentDOM.style.left = '0px';
        //         contentDOM.style.transform = 'none';
        //     } else {
        //         // 竖屏，这里微信应该由bug，我切换为竖屏的时候，width:375, height: 323,
        //         // 导致不能旋转角度。 在safari、chrome上是正确的。
        //         console.log('change to portrait')
        //         contentDOM.style.width = height + 'px';
        //         contentDOM.style.height = width + 'px';
        //         contentDOM.style.top = (height - width) / 2 + 'px';
        //         contentDOM.style.left = 0 - (height - width) / 2 + 'px';
        //         contentDOM.style.transform = 'rotate(90deg)';
        //     }
        //
        // }, false);

    });
</script>