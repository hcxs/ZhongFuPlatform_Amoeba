<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception */

use yii\helpers\Html;

$this->title = Yii::t('app.c2', 'User Sum Apply');
$this->params['back'] = true;
$this->params['header'] = true;
$this->params['title'] = Yii::t('app.c2', 'User Sum Apply');
?>
<div class="container-fluid">

    <div class="alert alert-success" style="margin-top: 10px">
        <?= nl2br(Html::encode($message)) ?>
    </div>

</div>
