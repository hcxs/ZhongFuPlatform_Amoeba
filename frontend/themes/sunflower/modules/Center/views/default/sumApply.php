<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/22 0022
 * Time: 上午 11:31
 */

use common\components\SmsCaptcha\Captcha;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use cza\base\widgets\ui\adminlte2\InfoBox;
use cza\base\models\statics\EntityModelStatus;

$this->title = Yii::t('app.c2', 'User Sum Apply');
$this->params['back'] = true;
$this->params['header'] = true;
$this->params['title'] = Yii::t('app.c2', 'User Sum Apply');
?>

    <style>
        .rule {
            margin-top: 40px;
            text-align: center;
            color: red
        }
    </style>

    <div class="container-fluid" style="margin-top: 10px">

        <div id="tip_body" style="display: none;" class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <p id="tip_content"></p>
        </div>


        <?php
        $form = ActiveForm::begin([
            'action' => ['sum-apply'],
            'options' => [
                'id' => $model->getBaseFormName(),
                'data-pjax' => true,
            ]]);
        ?>

        <?php
        echo Form::widget([
            'model' => $model,
            'form' => $form,
            'columns' => 1,
            'attributes' => [
                'type' => [
                    'type' => Form::INPUT_RADIO_LIST,
                    'items' => \common\models\c2\statics\UserSumApplyType::getHashMap('id', 'label'),
                    'options' => [
                        'inline' => true,
                        // 'onchange' => new \yii\web\JsExpression("function(){console.log(1)}"),
                    ]
                ],
                'account' => ['type' => Form::INPUT_TEXT, 'options' => ['readonly' => true, 'placeholder' => $model->getAttributeLabel('account')]],
                'bank_name' => ['type' => Form::INPUT_TEXT, 'options' => ['readonly' => true, 'placeholder' => $model->getAttributeLabel('bank_name')]],
                'bankcard_number' => ['type' => Form::INPUT_TEXT, 'options' => ['readonly' => true, 'placeholder' => $model->getAttributeLabel('bankcard_number')]],
                'apply_sum' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('apply_sum')]],
                'username' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('username')]],
                'mobile_number' => ['type' => Form::INPUT_TEXT, 'options' => ['placeholder' => $model->getAttributeLabel('mobile_number')]],
                'verify_code' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => Captcha::className(),
                    'label' => false,
                    'options' => [
                        'form' => $form,
                        'mobileId' => Html::getInputId($model, 'mobile_number'),
                    ],
                ],
            ]
        ]);

        ?>
        <?php

        echo Html::beginTag('div', ['class' => 'container-fluid']);
        echo Html::submitButton('<i class="fa fa-save"></i> ' . Yii::t('app.c2', 'Save'), ['type' => 'button', 'class' => 'btn btn-info btn-block']);
        echo Html::endTag('div');
        ?>

        <?php ActiveForm::end(); ?>

        <div class="container-fluid rule">
            <p><?= Yii::t('app.c2', 'Sum apply explain:') ?></p>
        </div>
    </div>

<?php
$typeBankcard = \common\models\c2\statics\UserSumApplyType::STATE_BANKCARD;
$onChangeJs = <<<JS

    $("input[name='UserSumApplyForm[type]']").on('change', function() {
        if ($(this).val() == '{$typeBankcard}') {
            $("input[name='UserSumApplyForm[bank_name]']").attr('readonly', false)
            $("input[name='UserSumApplyForm[bankcard_number]']").attr('readonly', false)
            $("input[name='UserSumApplyForm[account]']").attr('readonly', true)
        } else {
            $("input[name='UserSumApplyForm[account]']").attr('readonly', false)
             $("input[name='UserSumApplyForm[bank_name]']").attr('readonly', true)
            $("input[name='UserSumApplyForm[bankcard_number]']").attr('readonly', true)
        }
});

JS;

$this->registerJs($onChangeJs);
?>