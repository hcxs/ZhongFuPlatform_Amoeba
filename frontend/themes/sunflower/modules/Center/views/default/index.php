<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app.c2', 'User Center');
// $this->params['back'] = true;
$this->params['header'] = true;
$this->params['title'] = Yii::t('app.c2', 'User Center');

$user = Yii::$app->user->currentUser;
//print_r($user);

?>
<style>
    .user-header-bg {
        height: 120px;
        padding-top: 10px;
        /*line-height: 120px;*/
        text-align: center;
        background-image: linear-gradient(80deg, #a1c4fd 0%, #c2e9fb 100%);
        /*background-color: rgb(28, 16, 40);*/
    }

    .user-header {
        width: 60px;
        border-radius: 30px;
    }

    .user-name {
        color: white;
        margin: 10px 0 0 0;
    }

    .block-tab {
        text-align: center;
    }

</style>
<div class="user-header-bg">
        <img class="user-header" src="/images/icon/default_header.png" />
    <p id="username" class="user-name"><span id="greet"></span><?= Yii::$app->user->currentUser->username ?></p>
</div>

<div class="container-fluid">

    <div class="block row" style="margin: 10px 0 0 0;padding-top: 10px">
        <div class="col-xs-4 block-tab">
            <P><?= Yii::t('app.c2', 'Grand Total Profit') ?></P>
            <P><?= $user->profit ? $user->profit->income : '0.00' ?></P>
        </div>
        <div class="col-xs-4 block-tab">
            <P><?= Yii::t('app.c2', 'KPI UnConfirm') ?></P>
            <P><?= $user->getUnConfirmGradeCount() ?></P>
        </div>
        <div class="col-xs-4 block-tab">
            <P><?= Yii::t('app.c2', 'Profit UnConfirm') ?></P>
            <P><?= $user->getUnConfirmProfitItem() ?></P>
        </div>
    </div>

<!--    <div class="block" style="margin-top: 10px;">-->
<!--        <div class="block-content">-->
<!--            <a href="/center/default/profile" class="col-xs-10">--><?//= Yii::t('app.c2', 'My Profile') ?><!--</a>-->
<!--            <div class="col-xs-2" style="text-align: right"><img class="block-right" src="/images/icon/right.png"></div>-->
<!--        </div>-->
<!--    </div>-->

    <div class="block" style="margin-top: 10px;">
        <div class="block-content">
            <a href="/user-center/default/profit" class="col-xs-10"><?= Yii::t('app.c2', 'My Profit') ?></a>
            <div class="col-xs-2" style="text-align: right"><img class="block-right" src="/images/icon/right.png"></div>
        </div>
    </div>

    <div class="block" style="margin-top: 1px;">
        <div class="block-content">
            <a href="/user-center/default/sum-apply" class="col-xs-10"><?= Yii::t('app.c2', 'Sum Apply') ?></a>
            <div class="col-xs-2" style="text-align: right"><img class="block-right" src="/images/icon/right.png"></div>
        </div>
    </div>

    <div class="block" style="margin-top: 1px;">
        <div class="block-content">
            <a href="/user-center/default/sum-apply-record" class="col-xs-10"><?= Yii::t('app.c2', 'Sum Apply Record') ?></a>
            <div class="col-xs-2" style="text-align: right"><img class="block-right" src="/images/icon/right.png"></div>
        </div>
    </div>

    <?= Html::beginForm(['/site/logout'], 'post', ['style' => 'margin-top: 40px;']); ?>
    <?=
    Html::submitButton(
        Yii::t('app.c2', 'Logout {s1}', ['s1' => $user->username]),
        ['class' => 'btn btn-danger btn-block logout']
    )
    ?>
    <?= Html::endForm(); ?>

</div>



<script>
    $(function () {
        now = new Date(), hour = now.getHours()
        if (hour < 12) {
            $('#greet').html("凌晨好，")
        }
        else if (hour < 9) {
            $('#greet').html("早上好，")
        }
        else if (hour < 12) {
            $('#greet').html("上午好，")
        }
        else if (hour < 14) {
            $('#greet').html("中午好，")
        }
        else if (hour < 17) {
            $('#greet').html("下午好，")
        }
        else if (hour < 19) {
            $('#greet').html("傍晚好，")
        }
        else if (hour < 22) {
            $('#greet').html("晚上好，")
        }
        else {
            $('#greet').html("夜里好，")
        }
    })
</script>