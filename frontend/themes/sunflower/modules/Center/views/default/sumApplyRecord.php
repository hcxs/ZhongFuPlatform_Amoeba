<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/22 0022
 * Time: 下午 13:24
 */

use yii\widgets\ListView;

$this->title = Yii::t('app.c2', 'User Sum Apply Record');
$this->params['back'] = true;
$this->params['header'] = true;
$this->params['title'] = Yii::t('app.c2', 'User Sum Apply Record');
?>

<div class="container-fluid" style="margin-top: 10px">
    <?php

    echo ListView::widget([
        'dataProvider' => $dataProvider,
        // 'layout' => "{items}\n{pager}",
        'itemOptions' => ['class' => 'item media-list'],
        'options' => ['class' => 'list-view'],
        'itemView' => '_apply_item',
        'summary' => '',
        'pager' => [
            'class' => \common\widgets\Y2sp\ScrollPager::className(),
            // 'next' => '.next a',
            // 'triggerOffset' => 20,
            // 'spinnerTemplate' => '<div class="ias-spinner" style="text-align: center;"><img src="/images/home/loading.png"/></div>',
            'triggerTemplate' => '<div class="ias-spinner btn btn-block btn-link" style="text-align: center;">加载更多</div>',
        ]
    ]);

    ?>
</div>
