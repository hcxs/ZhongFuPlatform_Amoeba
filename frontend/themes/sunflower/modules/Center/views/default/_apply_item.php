<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/22 0022
 * Time: 上午 11:33
 */

?>
<style>
    .of {
        font-weight: bold;
        font-size: 18px;
        color: #a1c4fd;
    }

    .card {
        background-color: white;
        margin-bottom: 10px;
        padding: 10px;
    }
</style>

<div class="card">

    <div class="row">
        <div class="col-xs-8">
            <p class=""><?= Yii::t('app.c2', 'Apply Sum:') . $model->apply_sum ?></p>
            <p class=""><?= Yii::t('app.c2', 'Apply Type:') . \common\models\c2\statics\UserSumApplyType::getLabel($model->type) ?></p>
            <p class=""><?= Yii::t('app.c2', 'Memo:') ?></p>
        </div>

        <div class="col-xs-4" style="text-align: right"><span
                    class="label label-success"><?= \common\models\c2\statics\UserSumApplyState::getLabel($model->state) ?></span>
        </div>
    </div>


</div>

