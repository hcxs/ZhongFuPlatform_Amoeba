<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/22 0022
 * Time: 上午 11:33
 */

?>
<style>
    .of {
        font-weight: bold;
        font-size: 18px;
        color: #a1c4fd;
    }
    .card {
        background-color: white;
        margin-bottom: 10px;
        padding: 10px;
    }
</style>

<div class="card">

    <p class="of"><?= $model->amoeba->name ?></p>

    <div class="row">
        <p class="col-xs-6"><?= Yii::t('app.c2', 'Join User:') . $model->grade->joinUser->username ?></p>
        <p class="col-xs-6"><?= Yii::t('app.c2', 'Invite User:') . $model->grade->inviteUser->username ?></p>
    </div>
    <div>
        <p><?= Yii::t('app.c2', 'Income:') ?><span style="color: red"><?= $model->income ?></span></p>
        <p><?= Yii::t('app.c2', 'Commit At:') . $model->updated_at ?></p>
    </div>

<span class="label label-success"><?= \common\models\c2\statics\UserProfitItemState::getLabel($model->state) ?></span>

</div>

