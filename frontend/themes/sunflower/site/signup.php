<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\SignupForm */

use common\components\SmsCaptcha\Captcha;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\builder\Form;
\frontend\assets\ChartAsset::register($this);

$messageName = $model->getMessageName();
$this->title = Yii::t('app.c2', 'User Register');
?>
<style>

    .main {
        /*background: url("/images/common/blue20.png") fixed;*/
        background-image: linear-gradient(120deg, #a1c4fd 0%, #c2e9fb 100%);
        height: 100%;
        top: 0;
        margin: 0;
        /*background-size: cover;*/
    }
     .slidercaptcha {
         width: 100%;
     }

    .slidercaptcha .card-body {
        padding: 3rem;
        text-align: center;
    }

    .slidercaptcha canvas:first-child {
        border-radius: 4px;
        border: 1px solid #e6e8eb;
    }

    .slidercaptcha.card .card-header {
        background-image: none;
        text-align: center;
        padding-top:20px;
        /*background-color: rgba(0, 0, 0, 0.03);*/
    }

    .yzm-tp{width: 80%;background: #fff;height:150px;position: absolute;top: 230px;left: 10%;z-index: 999;display: none}
    /*display: none*/
    .tips-name{width: 100%;margin-bottom: 5px;}
    .tips-name span{color: red;}

</style>

<div class="container" style="margin-top: 10%">

    <h2 class="welcome"><?= Yii::t('app.c2', 'Welcome to signup') ?></h2>

    <?php
    $form = \kartik\widgets\ActiveForm::begin([
        'options' => [
            'action' => ['signup', 'ic' => $model->invite_code],
            'id' => $model->getBaseFormName(),
            'data-pjax' => false,
            // 'class' => 'form-horizontal'
            'style' => 'margin-top: 10%'
        ]]);
    ?>

    <div id="tip_body" style="display: none;" class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
        <p id="tip_content"></p>
    </div>

    <div class="tips-name">
        <span>*请填写真实名字</span>
    </div>

    <?php

    echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'username' => [
                'type' => Form::INPUT_TEXT,
                'label' => false,
                'options' => [
                    'placeholder' => $model->getAttributeLabel('username'),
                ]
            ],
            'mobile_number' => [
                'type' => Form::INPUT_TEXT,
                'label' => false,
                'options' => [
                    'placeholder' => $model->getAttributeLabel('mobile_number'),
                ]
            ],
            'password' => [
                'type' => Form::INPUT_PASSWORD,
                'label' => false,
                'options' => [
                    'placeholder' => $model->getAttributeLabel('password'),
                ]
            ],
            'invite_code' => [
                'type' => Form::INPUT_HIDDEN,
                'label' => false,
                'options' => [
                    'readonly' => true,
                    'placeholder' => $model->getAttributeLabel('invite_code'),
                ]
            ],
            'verify_code' => [
                'type' => Form::INPUT_WIDGET,
                'widgetClass' => Captcha::className(),
                'label' => false,
                'options' => [
                    'form' => $form,
                    'mobileId' => Html::getInputId($model, 'mobile_number'),
                ],
            ],

        ]
    ]);


    ?>

    <div class="container tr">
        <?php echo Html::submitButton(Yii::t('app.c2', 'Signup'), ['class' => 'btn btn-info btn-block font-white mb10']) ?>
        <?php echo Html::a(Yii::t('app.c2', 'Login account right now'), '/user/login', ['class' => '']) ?>
    </div>

    <div class="yzm-tp" id="yzm">
        <div class="container-fluid">
            <div class="form-row">
                <div class="col-12">
                    <div class="slidercaptcha card">
                        <div class="card-header">
                            <span>请完成安全验证</span>
                        </div>
                        <div class="card-body">
<!--                            <div id="captcha"></div>-->
                            <span id="secc">Loading...</span><input id="check" size="4" class="form-control" style="width:50px;display: inline-flex" /><input type="button" value="提交"  id="yzmtj" class="btn btn-info" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    \kartik\widgets\ActiveForm::end();
    ?>
</div>


<script language="JavaScript" type="text/javascript">
    var a=Math.round(parseInt(Math.random()*10));
    var b=Math.round(parseInt(Math.random()*10));
    document.getElementById('secc').innerHTML=a+"+"+b+"=";
    console.log(1);
    //var url=<?php //\yii\helpers\Url::toRoute($this->captchaAction) ?>

    document.getElementById('yzmtj').onclick=function(){
        var c=document.getElementById('check').value;
        if (c!=a+b) {
            // alert('错误');
            //location.href="http://www.qsyz.net/";
        } else {
            // alert('正确');
            $(".yzm-tp").css('display','none');
            var data = {mobile: $('#signupform-mobile_number').val()};
            // console.log(data)
            $.ajax({
                url:  '/register/sms-captcha',
                type: 'post',
                data: data,
                success: function(data) {
                    console.log(data)
                    // $('#tip_content').html(data._data.data);
                    // $('#tip_body').show();
                },
                error :function(XMLHttpRequest, textStatus, errorThrown){
                    // console.log(data);
                    console.log(XMLHttpRequest);
                }
            });
        }
    }
</script>
