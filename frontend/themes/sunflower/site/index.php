<?php

/* @var $this yii\web\View */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
$this->title = Yii::t('app.c2', 'Amoeba Form Select');
// $this->params['back'] = true;
$this->params['header'] = true;
$this->params['title'] = Yii::t('app.c2', 'Amoeba Form Select');

?>
<style>
    .box {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-evenly;
        align-content: space-evenly;
        width: 100%;
        margin-top: 10px;
        margin-bottom: 10px;
    }

    .box-item {
    }
</style>
<div class="container-fluid">

    <div class="box">
        <?php foreach ($models as $model):?>
            <a href="/amoeba-form" class="box-item btn btn-default btn-block">
                <p><?= $model->amoebaForm->name ?></p>
                <p><?= Yii::t('app.c2', 'Code:') . $model->seo_code ?></p>
            </a>

        <?php endforeach; ?>
    </div>




    <?//= Html::beginForm(['/site/logout'], 'post'); ?>
    <?//=
    //
    // Html::submitButton(
    //     Yii::t('app.c2', 'Logout {s1}', ['s1' => Yii::$app->user->identity->username]),
    //     ['class' => 'btn btn-danger btn-block logout']
    // )
    // ?>
    <?//= Html::endForm(); ?>

</div>
