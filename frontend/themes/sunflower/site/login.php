<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\SignupForm */

use common\components\SmsCaptcha\Captcha;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\builder\Form;
$this->title = Yii::t('app.c2', 'User Login');


$messageName = $model->getMessageName();
?>

<style>

    .main {
        /*background: url("/images/common/blue20.png") fixed;*/
        background-image: linear-gradient(120deg, #a1c4fd 0%, #c2e9fb 100%);
        height: 100%;
        top: 0;
        margin: 0;
        /*background-size: cover;*/
    }

    /*body {*/
        /*background: url("/images/common/blue20.png") fixed;*/
        /*background-size: cover;*/
    /*}*/

</style>



<!--<img style="width: 100%" src="/images/common/logo_org.jpg">-->
<div class="container" style="margin-top: 10%">

    <h2 class="welcome"><?= Yii::t('app.c2', 'Welcome to signup') ?></h2>


    <?php
    $form = \kartik\widgets\ActiveForm::begin([
        'options' => [
            'id' => $model->getBaseFormName(),
            'data-pjax' => false,
            // 'class' => 'form-horizontal'
            'style' => 'margin-top: 10%'
        ]]);
    ?>

    <?php

    echo Form::widget([
        'model' => $model,
        'form' => $form,
        'columns' => 1,
        'attributes' => [
            'mobile_number' => [
                'type' => Form::INPUT_TEXT,
                'label' => false,
                'fieldConfig' => [
                    // 'template' => '<label for="signupform-mobile_number" class="col-xs-2 control-label">' . $model->getAttributeLabel('mobile_number') . '</label>
                    //                 <div class="col-xs-10">{input}</div>',
                    // 'options' => ['class' => 'form-group row']
                ],
                'options' => [
                    'placeholder' => $model->getAttributeLabel('mobile_number'),
                    // 'class' => 'form-control-lg'
                ]
            ],
            'password' => [
                'type' => Form::INPUT_PASSWORD,
                'label' => false,
                'fieldConfig' => [
                    // 'template' => '<label for="signupform-password" class="col-xs-2 control-label">' . $model->getAttributeLabel('password') . '</label>
                    //                 <div class="col-xs-10">{input}</div>',
                    // 'options' => ['class' => 'form-group row']
                ],
                'options' => [
                    'placeholder' => $model->getAttributeLabel('password'),
                    // 'class' => 'form-control-lg'
                ]
            ],
        ]
    ]);
    ?>

    <?php echo Html::submitButton(Yii::t('app.c2', 'Login'), ['class' => 'btn btn-info btn-block font-white']) ?>
    <?php echo Html::a(Yii::t('app.c2', 'Forget Password'), '/user/forget-password', [
        'class' => 'btn btn-link',
        'style' => 'padding: 10px 0'
    ]) ?>

    <div style="text-align: center">
        <?php echo Html::a(Yii::t('app.c2', 'Signup Account'), '/user/signup') ?>
    </div>
    <?php
    \kartik\widgets\ActiveForm::end();
    ?>
</div>