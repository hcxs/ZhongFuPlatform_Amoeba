<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/3/22
 * Time: 15:47
 */

return [
    'home' => [
        'class' => 'frontend\modules\Home\Module',
    ],
    'amoeba-form' => [
        'class' => 'frontend\modules\AmoebaForm\Module',
    ],
    'study' => [
        'class' => 'frontend\modules\Study\Module',
    ],
    'user-center' => [
        'class' => 'frontend\modules\Center\Module',
    ],
    'amoeba' => [
        'class' => 'frontend\modules\Amoeba\Module',
    ],
    'cms' => [
        'class' => 'frontend\modules\CMS\Module',
    ],

    'log-reader' => [
        'class' => 'zhuravljov\yii\logreader\Module',
        'aliases' => [
            'backend Errors' => '@backend/runtime/logs/backend_debug.log',
            'backend Info' => '@backend/runtime/logs/backend_info.log',
            'Console Errors' => '@backend/runtime/logs/app.log',
        ],
    ],
];