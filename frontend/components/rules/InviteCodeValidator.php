<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/21 0021
 * Time: 下午 17:18
 */
namespace frontend\components\rules;

use common\models\c2\entity\UserInviteCode;
use Yii;
use yii\validators\Validator;

class InviteCodeValidator extends Validator
{

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if ($this->message === null) {
            $this->message = Yii::t('app.c2', 'The Recommend code is incorrect.');
        }
    }

    /**
     * @inheritdoc
     */
    protected function validateValue($value)
    {
        $model = UserInviteCode::findOne(['code' => $value]);
        if (!is_null($model) && !is_null($model->user)) {
            return [];
        }
        return [Yii::t('app.c2', 'The Recommend code is incorrect.'), []];
    }
}