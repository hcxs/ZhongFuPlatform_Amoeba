<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/6/22 0022
 * Time: 下午 12:26
 */

namespace frontend\models;


use common\components\SmsCaptcha\CaptchaValidator;
use common\models\c2\entity\FeUser;
use common\models\c2\entity\UserSumApply;
use common\models\c2\statics\UserSumApplyState;
use cza\base\models\ModelTrait;
use Yii;
use yii\base\Model;

class UserSumApplyForm extends Model
{
    use ModelTrait;
    public $type;
    public $user_id;
    public $apply_sum;
    public $username;
    public $mobile_number;
    public $bank_name;
    public $bankcard_number;
    public $account;
    public $verify_code;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'account'], 'string', 'min' => 2, 'max' => 255],
            [['apply_sum',], 'validateSum'],
            [['username', 'apply_sum', 'mobile_number', 'verify_code', 'type'], 'required'],
            [['mobile_number',], 'match', 'pattern' => '/^1[0-9]{10}$/', 'message' => Yii::t('app.c2', '{attribute} must be mobile format!')],
            [['mobile_number'], 'string', 'length' => 11],
            [['mobile_number'], 'number', 'integerOnly' => true],
            ['verify_code', CaptchaValidator::className()],
        ];
    }

    public function attributeLabels()
    {
        return [
            'type' => Yii::t('app.c2', 'Apply Type'),
            'mobile_number' => Yii::t('app.c2', 'Mobile Number'),
            'username' => Yii::t('app.c2', 'Username'),
            'verify_code' => Yii::t('app.c2', 'Sms code'),
            'apply_sum' => Yii::t('app.c2', 'Apply Sum'),
            'bank_name' => Yii::t('app.c2', 'Bank Name'),
            'bankcard_number' => Yii::t('app.c2', 'Bankcard Number'),
            'account' => Yii::t('app.c2', 'Account'),
        ];
    }

    public function validateSum($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = FeUser::findOne($this->user_id);
            $profit = $user->profit;
            $sumapplys = $user->getSumApply()->where(['state' => UserSumApplyState::STATE_INIT])->all();
            $sum = 0;
            foreach ($sumapplys as $sumapply) {
                $sum += $sumapply->apply_sum;
            }
            $canSum = $profit->income - $sum;
            if ($this->apply_sum > $profit->income) {
                $this->addError($attribute, Yii::t('app.c2',
                    'Sum apply can more {s1}.', ['s1' => $canSum]));
            }
        }
    }

    public function apply()
    {
        if (!$this->validate()) {
            return null;
        }
        $model = new UserSumApply();
        $model->loadDefaultValues();
        $model->type = $this->type;
        $model->user_id = $this->user_id;
        $model->apply_sum = $this->apply_sum;
        $model->bank_name = $this->bank_name;
        $model->bankcard_number = $this->bankcard_number;
        $model->username = $this->username;
        $model->mobile_number = $this->mobile_number;
        if ($model->save()) {
            return true;
        } else {
            Yii::info($model->errors);
        }
        return null;
    }

}