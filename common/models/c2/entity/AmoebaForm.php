<?php

namespace common\models\c2\entity;

use Yii;

/**
 * This is the model class for table "{{%amoeba_form}}".
 *
 * @property string $id
 * @property string $amoeba_id
 * @property integer $type
 * @property string $name
 * @property string $label
 * @property string $parent_id
 * @property integer $status
 * @property integer $position
 * @property string $created_at
 * @property string $updated_at
 */
class AmoebaForm extends \cza\base\models\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%amoeba_form}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'amoeba_id', 'parent_id', 'position'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['status'], 'integer', 'max' => 4],
            [['name', 'label'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app.c2', 'ID'),
            'amoeba_id' => Yii::t('app.c2', 'Amoeba ID'),
            'type' => Yii::t('app.c2', 'Type'),
            'name' => Yii::t('app.c2', 'Name'),
            'label' => Yii::t('app.c2', 'Label'),
            'parent_id' => Yii::t('app.c2', 'Parent ID'),
            'status' => Yii::t('app.c2', 'Status'),
            'position' => Yii::t('app.c2', 'Position'),
            'created_at' => Yii::t('app.c2', 'Created At'),
            'updated_at' => Yii::t('app.c2', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\c2\query\AmoebaFormQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\c2\query\AmoebaFormQuery(get_called_class());
    }
    
    /**
    * setup default values
    **/
    public function loadDefaultValues($skipIfSet = true) {
        parent::loadDefaultValues($skipIfSet);
    }

    public function getAmoebaFormItems()
    {
        return $this->hasMany(AmoebaFormItem::className(), ['amoeba_form_id' => 'id']);
    }

    public function getAmoebaFormItemUsersArr()
    {
        $q = $this->getAmoebaFormItems()->joinWith(['user']);
        $arr = $q->asArray()->all();
        return $arr;
    }

}
