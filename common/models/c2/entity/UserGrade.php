<?php

namespace common\models\c2\entity;

use backend\models\c2\entity\rbac\BeUser;
use common\models\c2\statics\UserGradeState;
use common\models\c2\statics\UserProfitItemState;
use Yii;

/**
 * This is the model class for table "{{%user_grade}}".
 *
 * @property string $id
 * @property string $amoeba_id
 * @property string $join_user_id
 * @property string $invite_user_id
 * @property string $amoeba_form_id
 * @property string $check_by
 * @property string $dues
 * @property integer $type
 * @property integer $state
 * @property integer $status
 * @property integer $position
 * @property string $created_at
 * @property string $updated_at
 */
class UserGrade extends \cza\base\models\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_grade}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amoeba_id', 'join_user_id', 'invite_user_id', 'amoeba_form_id', 'check_by', 'position'], 'integer'],
            [['dues'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['type', 'state', 'status'], 'integer', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app.c2', 'ID'),
            'amoeba_id' => Yii::t('app.c2', 'Amoeba ID'),
            'join_user_id' => Yii::t('app.c2', 'Join User ID'),
            'invite_user_id' => Yii::t('app.c2', 'Invite User ID'),
            'amoeba_form_id' => Yii::t('app.c2', 'Amoeba Form ID'),
            'check_by' => Yii::t('app.c2', 'Check By'),
            'dues' => Yii::t('app.c2', 'Dues'),
            'type' => Yii::t('app.c2', 'Type'),
            'state' => Yii::t('app.c2', 'State'),
            'status' => Yii::t('app.c2', 'Status'),
            'position' => Yii::t('app.c2', 'Position'),
            'created_at' => Yii::t('app.c2', 'Created At'),
            'updated_at' => Yii::t('app.c2', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\c2\query\UserGradeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\c2\query\UserGradeQuery(get_called_class());
    }

    /**
     * setup default values
     **/
    public function loadDefaultValues($skipIfSet = true)
    {
        parent::loadDefaultValues($skipIfSet);
    }

    public function getJoinUser()
    {
        return $this->hasOne(FeUser::className(), ['id' => 'join_user_id']);
    }

    public function getInviteUser()
    {
        return $this->hasOne(FeUser::className(), ['id' => 'invite_user_id']);
    }

    public function getChecker()
    {
        return $this->hasOne(BeUser::className(), ['id' => 'check_by']);
    }

    public function getAmoeba()
    {
        return $this->hasOne(Amoeba::className(), ['id' => 'amoeba_id']);
    }

    public function isStateInit()
    {
        return ($this->state == UserGradeState::STATE_INIT);
    }

    public function isStatePass()
    {
        return ($this->state == UserGradeState::STATE_PASS);
    }

    public function getUserProfitItem()
    {
        return $this->hasMany(UserProfitItem::className(), ['grade_id' => 'id']);
    }

    public function commitAssignment()
    {
        foreach ($this->userProfitItem as $item) {
            $user = $item->user;
            $userProfit = $user->profit;
            if (is_null($userProfit)) {
                $userProfit = new UserProfit();
                $userProfit->user_id = $item->user_id;
                $userProfit->loadDefaultValues();
            }
            $userProfit->income += $item->income;
            if ($userProfit->save()) {
                $item->updateAttributes(['state' => UserProfitItemState::STATE_COMMIT]);
            }
        }
        $this->updateAttributes(['state' => UserGradeState::STATE_FINISH]);
        return true;
    }

}
