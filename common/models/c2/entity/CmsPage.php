<?php

namespace common\models\c2\entity;

use common\behaviors\CmsContentBehavior;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%cms_page}}".
 *
 * @property integer $id
 * @property integer $type
 * @property string $seo_code
 * @property string $title
 * @property string $breadcrumb
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property string $content_heading
 * @property string $content
 * @property string $layout
 * @property string $custom_theme
 * @property string $access_role
 * @property integer $views_count
 * @property integer $comment_count
 * @property integer $is_homepage
 * @property integer $is_released
 * @property integer $is_draft
 * @property string $released_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $status
 * @property integer $position
 * @property string $created_at
 * @property string $updated_at
 */
class CmsPage extends \cza\base\models\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_page}}';
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'contentBehavior' => [
                'class' => CmsContentBehavior::className(),
                'fields' => ['content'],
                'config' => [
                    'entity_class' => static::className()
                ],
                // 'options' => [
                //     'renderWhenAttach' => false,
                //     'renderWhenAfterFind' => true,
                //     'renderImage' => true,
                //     'renderBlock' => false,
                //     'renderWidget' => false,
                //     'renderAlbumBlock' => false,
                //     'renderInternal' => false,
                // ],
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['meta_keywords', 'meta_description', 'content_heading', 'content'], 'string'],
            [['views_count', 'comment_count', 'created_by', 'updated_by', 'position'], 'integer'],
            [['released_at', 'created_at', 'updated_at'], 'safe'],
            [['type', 'is_homepage', 'is_released', 'is_draft', 'status'], 'integer', 'max' => 4],
            [['seo_code', 'title', 'breadcrumb', 'meta_title', 'layout', 'custom_theme', 'access_role'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app.c2', 'ID'),
            'type' => Yii::t('app.c2', 'Type'),
            'seo_code' => Yii::t('app.c2', 'Seo Code'),
            'title' => Yii::t('app.c2', 'Title'),
            'breadcrumb' => Yii::t('app.c2', 'Breadcrumb'),
            'meta_title' => Yii::t('app.c2', 'Meta Title'),
            'meta_keywords' => Yii::t('app.c2', 'Meta Keywords'),
            'meta_description' => Yii::t('app.c2', 'Meta Description'),
            'content_heading' => Yii::t('app.c2', 'Content Heading'),
            'content' => Yii::t('app.c2', 'Content'),
            'layout' => Yii::t('app.c2', 'Layout'),
            'custom_theme' => Yii::t('app.c2', 'Custom Theme'),
            'access_role' => Yii::t('app.c2', 'Access Role'),
            'views_count' => Yii::t('app.c2', 'Views Count'),
            'comment_count' => Yii::t('app.c2', 'Comment Count'),
            'is_homepage' => Yii::t('app.c2', 'Is Homepage'),
            'is_released' => Yii::t('app.c2', 'Is Released'),
            'is_draft' => Yii::t('app.c2', 'Is Draft'),
            'released_at' => Yii::t('app.c2', 'Released At'),
            'created_by' => Yii::t('app.c2', 'Created By'),
            'updated_by' => Yii::t('app.c2', 'Updated By'),
            'status' => Yii::t('app.c2', 'Status'),
            'position' => Yii::t('app.c2', 'Position'),
            'created_at' => Yii::t('app.c2', 'Created At'),
            'updated_at' => Yii::t('app.c2', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\c2\query\CmsPageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\c2\query\CmsPageQuery(get_called_class());
    }

    /**
     * setup default values
     **/
    public function loadDefaultValues($skipIfSet = true)
    {
        parent::loadDefaultValues($skipIfSet);
    }

    public function getUrl($baseUrl = '')
    {
        return !empty($baseUrl) ? $baseUrl . "/page/{$this->seo_code}" : "/page/{$this->seo_code}";
    }

    public static function getPreviewPage($seo_code)
    {
        return static::find()->active()->andWhere(['seo_code' => $seo_code])->one();
    }

    public function getPreviewUrl($baseUrl = '')
    {
        if (!isset($this->_data['PreviewUrl'])) {
            $this->_data['PreviewUrl'] = !empty($baseUrl) ? $baseUrl . Url::toRoute(["/cms/cms/preview", 'seo_code' => $this->seo_code]) : $this->_data['PreviewUrl'];
        }
        return $this->_data['PreviewUrl'];
    }

    public static function getValidPage($seo_code)
    {
        return static::find()->active()->released()->andWhere(['seo_code' => $seo_code])->one();
    }

    /**
     *
     * @return string
     */
    public function getCacheKey()
    {
        return $this->seo_code . '_' . Yii::$app->language;
    }

}
