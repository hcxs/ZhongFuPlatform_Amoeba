<?php

namespace common\models\c2\entity;

use common\behaviors\CmsContentBehavior;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%cms_block}}".
 *
 * @property string $id
 * @property integer $type
 * @property string $code
 * @property string $label
 * @property string $url
 * @property string $content
 * @property string $created_by
 * @property string $updated_by
 * @property integer $status
 * @property integer $position
 * @property string $created_at
 * @property string $updated_at
 */
class CmsBlock extends \cza\base\models\ActiveRecord
{
    use \common\traits\AttachmentTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%cms_block}}';
    }

    public function behaviors() {
        return ArrayHelper::merge(parent::behaviors(), [
            'contentBehavior' => [
                'class' => CmsContentBehavior::className(),
                'fields' => array('content'),
                //                        'config' => [
                //                            'entity_class' => static::className()
                //                        ],
            ],
            'attachmentsBehavior' => [
                'class' => \cza\base\modules\Attachments\behaviors\AttachmentBehavior::className(),
                'attributesDefinition' => [
                    'avatar' => [
                        'class' => EntityAttachmentImage::className(),
                        'validator' => 'image',
                        'rules' => [
                            'maxFiles' => 1,
                            'extensions' => Yii::$app->params['config']['upload']['imageWhiteExts'],
                            'maxSize' => Yii::$app->params['config']['upload']['maxFileSize'],
                        ]
                    ],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label'], 'required'],
            [['content'], 'string'],
            [['created_by', 'updated_by', 'position'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['type', 'status'], 'integer', 'max' => 4],
            [['code', 'label', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app.c2', 'ID'),
            'type' => Yii::t('app.c2', 'Type'),
            'code' => Yii::t('app.c2', 'Code'),
            'label' => Yii::t('app.c2', 'Label'),
            'url' => Yii::t('app.c2', 'Url'),
            'content' => Yii::t('app.c2', 'Content'),
            'created_by' => Yii::t('app.c2', 'Created By'),
            'updated_by' => Yii::t('app.c2', 'Updated By'),
            'status' => Yii::t('app.c2', 'Status'),
            'position' => Yii::t('app.c2', 'Position'),
            'created_at' => Yii::t('app.c2', 'Created At'),
            'updated_at' => Yii::t('app.c2', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\c2\query\CmsBlockQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\c2\query\CmsBlockQuery(get_called_class());
    }
    
    /**
    * setup default values
    **/
    public function loadDefaultValues($skipIfSet = true) {
        parent::loadDefaultValues($skipIfSet);
    }

    public static function getCmsFieldByCode($code, $field) {
        $model = static::findOne(['code' => $code]);
        if ($model) {
            $model->renderContent();
            return $model->$field;
        }
        return "";
    }

    public static function findOneThenRender($condition) {
        $model = static::findOne($condition);
        if ($model) {
            $model->renderContent();
        }
        return $model;
    }

}
