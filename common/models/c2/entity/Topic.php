<?php

namespace common\models\c2\entity;

use Yii;

/**
 * This is the model class for table "{{%topic}}".
 *
 * @property string $id
 * @property integer $type
 * @property string $code
 * @property string $title
 * @property string $render_definition
 * @property string $summary
 * @property string $content
 * @property integer $views_count
 * @property string $created_by
 * @property string $updated_by
 * @property integer $status
 * @property integer $position
 * @property string $created_at
 * @property string $updated_at
 */
class Topic extends \cza\base\models\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%topic}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['render_definition', 'summary', 'content'], 'string'],
            [['views_count', 'created_by', 'updated_by', 'position'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['type', 'status'], 'integer', 'max' => 4],
            [['code', 'title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app.c2', 'ID'),
            'type' => Yii::t('app.c2', 'Type'),
            'code' => Yii::t('app.c2', 'Code'),
            'title' => Yii::t('app.c2', 'Title'),
            'render_definition' => Yii::t('app.c2', 'Render Definition'),
            'summary' => Yii::t('app.c2', 'Summary'),
            'content' => Yii::t('app.c2', 'Content'),
            'views_count' => Yii::t('app.c2', 'Views Count'),
            'created_by' => Yii::t('app.c2', 'Created By'),
            'updated_by' => Yii::t('app.c2', 'Updated By'),
            'status' => Yii::t('app.c2', 'Status'),
            'position' => Yii::t('app.c2', 'Position'),
            'created_at' => Yii::t('app.c2', 'Created At'),
            'updated_at' => Yii::t('app.c2', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\c2\query\TopicQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\c2\query\TopicQuery(get_called_class());
    }
    
    /**
    * setup default values
    **/
    public function loadDefaultValues($skipIfSet = true) {
        parent::loadDefaultValues($skipIfSet);
    }

}
