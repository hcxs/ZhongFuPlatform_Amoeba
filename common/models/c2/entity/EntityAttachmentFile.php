<?php

namespace common\models\c2\entity;

use cza\base\models\statics\EntityModelState;
use Yii;
use common\models\c2\statics\EntityAttachmentType;

class EntityAttachmentFile extends EntityAttachment {

    public function loadDefaultValues($skipIfSet = true) {
        parent::loadDefaultValues($skipIfSet);
        $this->type = EntityAttachmentType::TYPE_FILE;
    }

    public function getStoreUrl() {
        if (isset($this->_data['storeUrl'])) {
            return $this->_data['storeUrl'];
        }

        if (Yii::$app->settings->get('url\cdn_enable') == EntityModelState::STATUS_ENABLE) {
            if ($this->isVideo()) {
                $cdn = empty(Yii::$app->settings->get('url\cdn_video_base_url')) ? Yii::$app->settings->get('url\cdn_image_base_url') : Yii::$app->settings->get('url\cdn_video_base_url');
                $this->_data['storeUrl'] = !empty($cdn) ? $cdn : parent::getStoreUrl();
            } else {
                $this->_data['storeUrl'] = !empty(Yii::$app->settings->get('url\cdn_image_base_url')) ? Yii::$app->settings->get('url\cdn_image_base_url') : parent::getStoreUrl();
            }
        }

        return parent::getStoreUrl();
    }

    public function getFileUrl() {
        if (!isset($this->_data['fileUrl'])) {
            $this->_data['fileUrl'] = $this->getStoreUrl() . '/' . $this->getLogicPath() . $this->getFile();
        }
        return $this->_data['fileUrl'];
    }

    public function getFile() {
        if (!isset($this->_data['file'])) {
            $this->_data['file'] = $this->hash . '.' . $this->extension;
        }
        return $this->_data['file'];
    }

}
