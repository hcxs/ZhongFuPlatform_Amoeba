<?php

namespace common\models\c2\entity;

use Yii;

/**
 * This is the model class for table "{{%amoeba_group}}".
 *
 * @property string $id
 * @property string $amoeba_id
 * @property string $children_amoeba_id
 * @property string $parent_amoeba_id
 * @property integer $type
 * @property integer $state
 * @property integer $status
 * @property integer $position
 * @property string $created_at
 * @property string $updated_at
 */
class AmoebaGroup extends \cza\base\models\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%amoeba_group}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amoeba_id', 'children_amoeba_id', 'parent_amoeba_id', 'position'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['type', 'state', 'status'], 'integer', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app.c2', 'ID'),
            'amoeba_id' => Yii::t('app.c2', 'Amoeba ID'),
            'children_amoeba_id' => Yii::t('app.c2', 'Children Amoeba ID'),
            'parent_amoeba_id' => Yii::t('app.c2', 'Parent Amoeba ID'),
            'type' => Yii::t('app.c2', 'Type'),
            'state' => Yii::t('app.c2', 'State'),
            'status' => Yii::t('app.c2', 'Status'),
            'position' => Yii::t('app.c2', 'Position'),
            'created_at' => Yii::t('app.c2', 'Created At'),
            'updated_at' => Yii::t('app.c2', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\c2\query\AmoebaGroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\c2\query\AmoebaGroupQuery(get_called_class());
    }
    
    /**
    * setup default values
    **/
    public function loadDefaultValues($skipIfSet = true) {
        parent::loadDefaultValues($skipIfSet);
    }

    public function getChildrenAmoeba()
    {
        return $this->hasOne(Amoeba::className(), ['id' => 'children_amoeba_id']);
    }

    public function getParentAmoeba()
    {
        return $this->hasOne(Amoeba::className(), ['id' => 'parent_amoeba_id']);
    }

}
