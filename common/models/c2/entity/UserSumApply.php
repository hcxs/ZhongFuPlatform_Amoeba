<?php

namespace common\models\c2\entity;

use common\models\c2\statics\UserSumApplyState;
use Yii;

/**
 * This is the model class for table "{{%user_sum_apply}}".
 *
 * @property string $id
 * @property integer $type
 * @property string $user_id
 * @property integer $apply_sum
 * @property string $bank_name
 * @property string $hash
 * @property string $confirmed_at
 * @property string $confirmed_by
 * @property string $username
 * @property string $mobile_number
 * @property string $bankcard_number
 * @property string $transfer_rate
 * @property string $received_sum
 * @property string $account
 * @property string $memo
 * @property integer $state
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class UserSumApply extends \cza\base\models\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_sum_apply}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'confirmed_by'], 'integer'],
            [['apply_sum', 'transfer_rate', 'received_sum'], 'number'],
            [['confirmed_at', 'created_at', 'updated_at'], 'safe'],
            [['memo'], 'string'],
            [['type', 'state', 'status'], 'integer', 'max' => 4],
            [['bank_name', 'hash', 'username', 'mobile_number', 'bankcard_number', 'account'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app.c2', 'ID'),
            'type' => Yii::t('app.c2', 'Type'),
            'user_id' => Yii::t('app.c2', 'User ID'),
            'apply_sum' => Yii::t('app.c2', 'Apply Sum'),
            'bank_name' => Yii::t('app.c2', 'Bank Name'),
            'hash' => Yii::t('app.c2', 'Hash'),
            'confirmed_at' => Yii::t('app.c2', 'Confirmed At'),
            'confirmed_by' => Yii::t('app.c2', 'Confirmed By'),
            'username' => Yii::t('app.c2', 'Username'),
            'mobile_number' => Yii::t('app.c2', 'Mobile Number'),
            'bankcard_number' => Yii::t('app.c2', 'Bankcard Number'),
            'transfer_rate' => Yii::t('app.c2', 'Transfer Rate'),
            'received_sum' => Yii::t('app.c2', 'Received Sum'),
            'account' => Yii::t('app.c2', 'Account'),
            'memo' => Yii::t('app.c2', 'Memo'),
            'state' => Yii::t('app.c2', 'State'),
            'status' => Yii::t('app.c2', 'Status'),
            'created_at' => Yii::t('app.c2', 'Created At'),
            'updated_at' => Yii::t('app.c2', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\c2\query\UserSumApplyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\c2\query\UserSumApplyQuery(get_called_class());
    }
    
    /**
    * setup default values
    **/
    public function loadDefaultValues($skipIfSet = true) {
        parent::loadDefaultValues($skipIfSet);
    }

    public function isStateInit()
    {
        return ($this->state == UserSumApplyState::STATE_INIT);
    }

    public function getUser()
    {
        return $this->hasOne(FeUser::className(), ['id' => 'user_id']);
    }

    public function setStateFinish()
    {
        $profit = $this->user->profit;
        $remain = $profit->income - $this->received_sum;
        $profit->income = $remain;
        if ($profit->save()) {
            $this->updateAttributes(['state' => UserSumApplyState::STATE_FINISH]);
            return true;
        }
        return null;
    }

}
