<?php

namespace common\models\c2\entity;

use Yii;

/**
 * This is the model class for table "{{%user_amoeba_rs}}".
 *
 * @property string $id
 * @property string $amoeba_id
 * @property string $user_id
 * @property integer $position
 * @property integer $state
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class UserAmoebaRs extends \cza\base\models\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_amoeba_rs}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amoeba_id', 'user_id', 'position'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['state', 'status'], 'integer', 'max' => 4],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app.c2', 'ID'),
            'amoeba_id' => Yii::t('app.c2', 'Amoeba ID'),
            'user_id' => Yii::t('app.c2', 'User ID'),
            'position' => Yii::t('app.c2', 'Position'),
            'state' => Yii::t('app.c2', 'State'),
            'status' => Yii::t('app.c2', 'Status'),
            'created_at' => Yii::t('app.c2', 'Created At'),
            'updated_at' => Yii::t('app.c2', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\c2\query\UserAmoebaRsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\c2\query\UserAmoebaRsQuery(get_called_class());
    }
    
    /**
    * setup default values
    **/
    public function loadDefaultValues($skipIfSet = true) {
        parent::loadDefaultValues($skipIfSet);
    }

}
