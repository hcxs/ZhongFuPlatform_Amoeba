<?php

namespace common\models\c2\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\c2\entity\AmoebaGroup;

/**
 * AmoebaGroupSearch represents the model behind the search form about `common\models\c2\entity\AmoebaGroup`.
 */
class AmoebaGroupSearch extends AmoebaGroup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'amoeba_id', 'children_amoeba_id', 'parent_amoeba_id', 'position'], 'integer'],
            [['type', 'state', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AmoebaGroup::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'sortParam' => $this->getSortParamName(),
            ],
            'pagination' => [
                'pageParam' => $this->getPageParamName(),
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'amoeba_id' => $this->amoeba_id,
            'children_amoeba_id' => $this->children_amoeba_id,
            'parent_amoeba_id' => $this->parent_amoeba_id,
            'position' => $this->position,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
    
    public function getPageParamName($splitor = '-'){
        $name = "AmoebaGroupPage";
        return \Yii::$app->czaHelper->naming->toSplit($name);
    }
    
    public function getSortParamName($splitor = '-'){
        $name = "AmoebaGroupSort";
        return \Yii::$app->czaHelper->naming->toSplit($name);
    }
}
