<?php

namespace common\models\c2\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\c2\entity\CmsPage;

/**
 * CmsPageSearch represents the model behind the search form about `common\models\c2\entity\CmsPage`.
 */
class CmsPageSearch extends CmsPage
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'views_count', 'comment_count', 'created_by', 'updated_by', 'position'], 'integer'],
            [['type', 'seo_code', 'title', 'breadcrumb', 'meta_title', 'meta_keywords', 'meta_description', 'content_heading', 'content', 'layout', 'custom_theme', 'access_role', 'is_homepage', 'is_released', 'is_draft', 'released_at', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CmsPage::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'sortParam' => $this->getSortParamName(),
            ],
            'pagination' => [
                'pageParam' => $this->getPageParamName(),
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'views_count' => $this->views_count,
            'comment_count' => $this->comment_count,
            'released_at' => $this->released_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'position' => $this->position,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'seo_code', $this->seo_code])
            ->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'breadcrumb', $this->breadcrumb])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description])
            ->andFilterWhere(['like', 'content_heading', $this->content_heading])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'layout', $this->layout])
            ->andFilterWhere(['like', 'custom_theme', $this->custom_theme])
            ->andFilterWhere(['like', 'access_role', $this->access_role])
            ->andFilterWhere(['like', 'is_homepage', $this->is_homepage])
            ->andFilterWhere(['like', 'is_released', $this->is_released])
            ->andFilterWhere(['like', 'is_draft', $this->is_draft])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
    
    public function getPageParamName($splitor = '-'){
        $name = "CmsPagePage";
        return \Yii::$app->czaHelper->naming->toSplit($name);
    }
    
    public function getSortParamName($splitor = '-'){
        $name = "CmsPageSort";
        return \Yii::$app->czaHelper->naming->toSplit($name);
    }
}
