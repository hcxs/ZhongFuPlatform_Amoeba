<?php

namespace common\models\c2\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\c2\entity\AmoebaFormItem;

/**
 * AmoebaFormItemSearch represents the model behind the search form about `common\models\c2\entity\AmoebaFormItem`.
 */
class AmoebaFormItemSearch extends AmoebaFormItem
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'amoeba_id', 'amoeba_form_id', 'user_id', 'position'], 'integer'],
            [['seo_code', 'label', 'state', 'status', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AmoebaFormItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'sortParam' => $this->getSortParamName(),
            ],
            'pagination' => [
                'pageParam' => $this->getPageParamName(),
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'amoeba_id' => $this->amoeba_id,
            'amoeba_form_id' => $this->amoeba_form_id,
            'user_id' => $this->user_id,
            'position' => $this->position,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'seo_code', $this->seo_code])
            ->andFilterWhere(['like', 'label', $this->label])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
    
    public function getPageParamName($splitor = '-'){
        $name = "AmoebaFormItemPage";
        return \Yii::$app->czaHelper->naming->toSplit($name);
    }
    
    public function getSortParamName($splitor = '-'){
        $name = "AmoebaFormItemSort";
        return \Yii::$app->czaHelper->naming->toSplit($name);
    }
}
