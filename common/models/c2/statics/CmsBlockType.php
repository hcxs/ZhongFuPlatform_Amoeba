<?php

namespace common\models\c2\statics;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * FeUserType
 *
 * @author ben
 */
class CmsBlockType extends AbstractStaticClass {

    const TYPE_GENERIC = 0;
    const TYPE_ALBUM = 1;
    const TYPE_ALBUM_LAYOUT = 2;
    

    protected static $_data;

    /**
     * 
     * @param type $id
     * @param type $attr
     * @return string|array
     */
    public static function getData($id = '', $attr = '') {
        if (is_null(static::$_data)) {
            static::$_data = [
                static::TYPE_GENERIC => ['id' => static::TYPE_GENERIC, 'label' => Yii::t('app.c2', 'Cms GenericBlock')],
                static::TYPE_ALBUM => ['id' => static::TYPE_ALBUM, 'label' => Yii::t('app.c2', 'Cms AlbumBlock')],
                static::TYPE_ALBUM_LAYOUT => ['id' => static::TYPE_ALBUM_LAYOUT, 'label' => Yii::t('app.c2', 'Cms AlbumLayoutBlock')],
            ];
        }
        
        if ($id !== '' && !empty($attr)) {
            return static::$_data[$id][$attr];
        }
        if ($id !== '' && empty($attr)) {
            return static::$_data[$id];
        }
        return static::$_data;
    }

    public static function getLabel($id) {
        return static::getData($id, 'label');
    }

    public static function getHashMap($keyField, $valField) {
        $key = __CLASS__ . Yii::$app->language . $keyField . $valField;
        $data = Yii::$app->cache->get($key);

        if ($data === false) {
            $data = ArrayHelper::map(static::getData(), $keyField, $valField);
            Yii::$app->cache->set($key, $data);
        }

        return $data;
    }

}
