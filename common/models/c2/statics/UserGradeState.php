<?php

namespace common\models\c2\statics;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * ConfigType
 *
 * @author ben
 */
class UserGradeState extends AbstractStaticClass {

    const STATE_INIT = 1;  // load in when demand
    const STATE_PASS = 2;  // load in when demand
    const STATE_FINISH = 3;  // load in when demand

    protected static $_data;

    /**
     * 
     * @param type $id
     * @param type $attr
     * @return string|array
     */
    public static function getData($id = '', $attr = '') {
        if (is_null(static::$_data)) {
            static::$_data = [
                static::STATE_INIT => ['id' => static::STATE_INIT, 'label' => Yii::t('app.c2', 'Init')],
                static::STATE_PASS => ['id' => static::STATE_PASS, 'label' => Yii::t('app.c2', 'Pass')],
                static::STATE_FINISH => ['id' => static::STATE_FINISH, 'label' => Yii::t('app.c2', 'Finish')],
            ];
        }
        if ($id !== '' && !empty($attr)) {
            return static::$_data[$id][$attr];
        }
        if ($id !== '' && empty($attr)) {
            return static::$_data[$id];
        }
        return static::$_data;
    }

}
