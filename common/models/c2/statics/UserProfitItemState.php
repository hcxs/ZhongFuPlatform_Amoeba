<?php

namespace common\models\c2\statics;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * ConfigType
 *
 * @author ben
 */
class UserProfitItemState extends AbstractStaticClass {

    const STATE_UNCONFIRM = 1;  // load in when demand
    const STATE_COMMIT = 2;  // load in when demand

    protected static $_data;

    /**
     * 
     * @param type $id
     * @param type $attr
     * @return string|array
     */
    public static function getData($id = '', $attr = '') {
        if (is_null(static::$_data)) {
            static::$_data = [
                static::STATE_UNCONFIRM => ['id' => static::STATE_UNCONFIRM, 'label' => Yii::t('app.c2', 'UnConfirm')],
                static::STATE_COMMIT => ['id' => static::STATE_COMMIT, 'label' => Yii::t('app.c2', 'Commit')],
            ];
        }
        if ($id !== '' && !empty($attr)) {
            return static::$_data[$id][$attr];
        }
        if ($id !== '' && empty($attr)) {
            return static::$_data[$id];
        }
        return static::$_data;
    }

}
