<?php

namespace common\models\c2\statics;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * ConfigType
 *
 * @author ben
 */
class UserSumApplyState extends AbstractStaticClass {

    const STATE_INIT = 1;  // load in when demand
    const STATE_REJECT = 2;  // load in when demand
    const STATE_TURNING = 3;  // load in when demand
    const STATE_FINISH = 4;  // load in when demand

    protected static $_data;

    /**
     * 
     * @param type $id
     * @param type $attr
     * @return string|array
     */
    public static function getData($id = '', $attr = '') {
        if (is_null(static::$_data)) {
            static::$_data = [
                static::STATE_INIT => ['id' => static::STATE_INIT, 'label' => Yii::t('app.c2', 'Apply Init')],
                static::STATE_TURNING => ['id' => static::STATE_TURNING, 'label' => Yii::t('app.c2', 'Turning')],
                static::STATE_REJECT => ['id' => static::STATE_REJECT, 'label' => Yii::t('app.c2', 'Reject')],
                static::STATE_FINISH => ['id' => static::STATE_FINISH, 'label' => Yii::t('app.c2', 'Finish')],
            ];
        }
        if ($id !== '' && !empty($attr)) {
            return static::$_data[$id][$attr];
        }
        if ($id !== '' && empty($attr)) {
            return static::$_data[$id];
        }
        return static::$_data;
    }

    public static function getData2($id = '', $attr = '') {
        if (is_null(static::$_data)) {
            static::$_data = [
                static::STATE_INIT => ['id' => static::STATE_INIT, 'label' => Yii::t('app.c2', 'Apply Init')],
                static::STATE_REJECT => ['id' => static::STATE_REJECT, 'label' => Yii::t('app.c2', 'Reject')],
            ];
        }
        if ($id !== '' && !empty($attr)) {
            return static::$_data[$id][$attr];
        }
        if ($id !== '' && empty($attr)) {
            return static::$_data[$id];
        }
        return static::$_data;
    }

    public static function getHashMap2($keyField, $valField)
    {
        $key = static::class . Yii::$app->language . $keyField . $valField;
        $data = Yii::$app->cache->get($key);

        if ($data === false) {
            $data = ArrayHelper::map(static::getData2(), $keyField, $valField);
            Yii::$app->cache->set($key, $data);
        }

        return $data;
    }

}
