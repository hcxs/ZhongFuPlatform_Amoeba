<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/3/26
 * Time: 17:42
 */

namespace common\models\c2\statics;
use Yii;

/**
 *
 * Class AmoebaType
 * @package common\models\c2\statics
 */
class AmoebaFormType extends AbstractStaticClass
{
    const TYPE_C3 = 100;  // load in when demand
    const TYPE_C2 = 90;  // load in when demand
    const TYPE_C1 = 80;  // load in when demand
    const TYPE_B = 70;  // load in when demand
    const TYPE_A = 60;  // load in when demand
    const TYPE_P = 50;  // load in when demand

    protected static $_data;

    /**
     *
     * @param type $id
     * @param type $attr
     * @return string|array
     */

    /**
     * 添加成员
     * @param string $id
     * @param string $attr
     * @return array|string
     */
    public static function getData($id = '', $attr = '') {
        if (is_null(static::$_data)) {
            static::$_data = [

                //static::TYPE_C3 => ['id' => static::TYPE_C3, 'label' => Yii::t('app.c2', 'Amoeba Form Type C3')],

                static::TYPE_C2 => ['id' => static::TYPE_C2, 'label' => Yii::t('app.c2', 'Amoeba Form Type C2')],
                static::TYPE_C1 => ['id' => static::TYPE_C1, 'label' => Yii::t('app.c2', 'Amoeba Form Type C1')],
                static::TYPE_B => ['id' => static::TYPE_B, 'label' => Yii::t('app.c2', 'Amoeba Form Type B')],
                static::TYPE_A => ['id' => static::TYPE_A, 'label' => Yii::t('app.c2', 'Amoeba Form Type A')],
                static::TYPE_P => ['id' => static::TYPE_P, 'label' => Yii::t('app.c2', 'Amoeba Form Type P')],
            ];
        }
        if ($id !== '' && !empty($attr)) {
            return static::$_data[$id][$attr];
        }
        if ($id !== '' && empty($attr)) {
            return static::$_data[$id];
        }
        return static::$_data;
    }

}