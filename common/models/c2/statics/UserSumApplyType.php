<?php

namespace common\models\c2\statics;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * ConfigType
 *
 * @author ben
 */
class UserSumApplyType extends AbstractStaticClass {

    const STATE_BANKCARD = 1;  // load in when demand
    const STATE_ALIPAY = 2;  // load in when demand
    const STATE_WECHAT = 3;  // load in when demand

    protected static $_data;

    /**
     * 
     * @param type $id
     * @param type $attr
     * @return string|array
     */
    public static function getData($id = '', $attr = '') {
        if (is_null(static::$_data)) {
            static::$_data = [
                static::STATE_BANKCARD => ['id' => static::STATE_BANKCARD, 'label' => Yii::t('app.c2', 'Bankcard')],
                static::STATE_WECHAT => ['id' => static::STATE_WECHAT, 'label' => Yii::t('app.c2', 'Wechat')],
                static::STATE_ALIPAY => ['id' => static::STATE_ALIPAY, 'label' => Yii::t('app.c2', 'Ali Pay')],
            ];
        }
        if ($id !== '' && !empty($attr)) {
            return static::$_data[$id][$attr];
        }
        if ($id !== '' && empty($attr)) {
            return static::$_data[$id];
        }
        return static::$_data;
    }

}
