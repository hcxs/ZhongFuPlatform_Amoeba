<?php

namespace common\models\c2\statics;

use Yii;

/**
 * TopicArticleType
 *
 * @author ben
 */
class TopicArticleType extends AbstractStaticClass {

    const TYPE_DEFAULT = 1;  // load in when demand
    const TYPE_FILE = 2;  // wechat article
    const TYPE_VIDEO = 3;
    
    protected static $_data;

    /**
     * 
     * @param type $id
     * @param type $attr
     * @return string|array
     */
    public static function getData($id = '', $attr = '') {
        if (is_null(static::$_data)) {
            static::$_data = [
                static::TYPE_DEFAULT => [
                    'id' => static::TYPE_DEFAULT,
                    'label' => Yii::t('app.c2', 'Default'),
                    'editUrl' => 'edit',
                ],
                static::TYPE_FILE => [
                    'id' => static::TYPE_FILE,
                    'label' => Yii::t('app.c2', 'File'),
                    'editUrl' => 'file-article/edit',
                ],
                static::TYPE_VIDEO => [
                    'id' => static::TYPE_VIDEO,
                    'label' => Yii::t('app.c2', 'Video'),
                    'editUrl' => 'edit',
                ]
            ];
        }
        if ($id !== '' && !empty($attr)) {
            return static::$_data[$id][$attr];
        }
        if ($id !== '' && empty($attr)) {
            return static::$_data[$id];
        }
        return static::$_data;
    }

    public static function getDefaultLabel() {
        return static::getLabel(self::TYPE_DEFAULT);
    }

    public static function getFileLabel() {
        return static::getLabel(self::TYPE_FILE);
    }

    public static function getVideosLabel() {
        return static::getLabel(self::TYPE_VIDEO);
    }

}
