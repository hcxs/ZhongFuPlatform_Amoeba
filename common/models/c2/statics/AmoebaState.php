<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2019/3/26
 * Time: 17:42
 */

namespace common\models\c2\statics;
use Yii;

/**
 *
 * Class AmoebaType
 * @package common\models\c2\statics
 */
class AmoebaState extends AbstractStaticClass
{
    const TYPE_INIT = 1;  // load in when demand
    const TYPE_UNKNOWN = 2;  // load in when demand
    const TYPE_COMPLETE = 3;  // load in when demand
    const TYPE_FINISH = 4;  // load in when demand

    protected static $_data;

    /**
     *
     * @param type $id
     * @param type $attr
     * @return string|array
     */
    public static function getData($id = '', $attr = '') {
        if (is_null(static::$_data)) {
            static::$_data = [
                static::TYPE_INIT => ['id' => static::TYPE_INIT, 'label' => Yii::t('app.c2', 'Init')],
                static::TYPE_UNKNOWN => ['id' => static::TYPE_UNKNOWN, 'label' => Yii::t('app.c2', 'Unknown')],
                static::TYPE_COMPLETE => ['id' => static::TYPE_COMPLETE, 'label' => Yii::t('app.c2', 'Complete')],
                static::TYPE_FINISH => ['id' => static::TYPE_FINISH, 'label' => Yii::t('app.c2', 'Finish')],
            ];
        }
        if ($id !== '' && !empty($attr)) {
            return static::$_data[$id][$attr];
        }
        if ($id !== '' && empty($attr)) {
            return static::$_data[$id];
        }
        return static::$_data;
    }

}