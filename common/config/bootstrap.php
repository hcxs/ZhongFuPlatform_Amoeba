<?php

if (YII_ENV_DEV) {
    define('BACKEND_BASE_URL', 'http://be-zhongfu-amoeba.tunnel.echomod.cn');
    define('FRONTEND_BASE_URL', 'http://fe-zhongfu-amoeba.tunnel.echomod.cn');
    define('IMAGE_BASE_URL', 'http://img-zhongfu-amoeba.tunnel.echomod.cn');
} else {
    define('BACKEND_BASE_URL', 'http://admin.zhongfu-chen.com');
    define('FRONTEND_BASE_URL', 'http://m.zhongfu-chen.com');
    define('IMAGE_BASE_URL', 'http://img.zhongfu-chen.com');
}

//define('BACKEND_BASE_URL', 'https://be-apollo-pre.kebiyj.com');
//define('FRONTEND_BASE_URL', 'https://fe-apollo-pre.kebiyj.com');
define('ESHOP_BASE_URL', 'https://eshop-apollo-pre.kebiyj.com');
//define('IMAGE_BASE_URL', 'https://img-apollo-pre.kebiyj.com');

Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@appimage', dirname(dirname(__DIR__)) . '/app_image/uploads');
Yii::setAlias('@app_eshop', dirname(dirname(__DIR__)) . '/app_eshop');
