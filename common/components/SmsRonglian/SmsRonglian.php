<?php
/*
 *   http://www.yuntongxun.com
 */
namespace common\components\SmsRonglian;

use Yii;
//use yii\base\Action;

class SmsRonglian
{

    public $Filename="./log.txt"; //日志文件
    public $Handle;

//    public function __construct($ServerIP=null,$ServerPort=null,$SoftVersion=null){
//        $this->Batch = date("YmdHis");
//        if($ServerIP) $this->ServerIP = $ServerIP;
//        if($ServerPort) $this->ServerPort = $ServerPort;
//        if($SoftVersion) $this->SoftVersion = $SoftVersion;
//        $this->Handle = fopen($this->Filename, 'a');
//    }

    /**
     * 设置主帐号
     *
     * @param AccountSid 主帐号
     * @param AccountToken 主帐号Token
     */
    public function setAccount($AccountSid=null,$AccountToken=null){
        if($AccountSid) $this->AccountSid = $AccountSid;
        if($AccountToken) $this->AccountToken = $AccountToken;
    }


    /**
     * 设置应用ID
     *
     * @param AppId 应用ID
     */
    public function setAppId($AppId=null){
        if($AppId) $this->AppId = $AppId;
    }

    /**
     * 打印日志
     *
     * @param log 日志内容
     */
    public function showlog($log){
        if($this->enabeLog){
            fwrite($this->Handle,$log."\n");
        }
    }

    /**
     * 发起HTTPS请求
     */
    public function curl_post($url,$data,$header,$post=1)
    {
        //初始化curl
        $ch = curl_init();
//        //参数设置
        $res= curl_setopt ($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt ($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, $post);
        if($post)
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
        $result = curl_exec ($ch);
        return $result;
        //连接失败
//        if($result == FALSE){
//            if($this->BodyType=='json'){
//                $result = "{\"statusCode\":\"172001\",\"statusMsg\":\"网络错误\"}";
//            } else {
/*                $result = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><Response><statusCode>172001</statusCode><statusMsg>网络错误</statusMsg></Response>";*/
//            }
//        }
//
//        curl_close($ch);
//        return $result;
    }



    /**
     * 发送模板短信
     * @param to 短信接收彿手机号码集合,用英文逗号分开
     * @param datas 内容数据
     * @param $tempId 模板Id
     */
    public function sendTemplateSMS($to,$datas,$tempId)
    {

//        //主帐号鉴权信息验证，对必选参数进行判空。
//        $auth=$this->accAuth();
//        if($auth!=""){
//            return $auth;
//        }
        // 拼接请求包体

//        const AccountSid = '8aaf070863f8fb0401641c2cec29192d';
//        const AccountToken = 'ec40ffdc5e7644c8a248d6f363009b45';
//        const AppId = '8aaf070863f8fb0401641c2cec7d1933';
//        const ServerIP = 'app.cloopen.com';
//        const ServerPort = '8883';
//        const SoftVersion = '2013-12-26';
//        const Batch=0;  //时间戳
//        const BodyType = "json";//包体格式，可填值：json 、xml
//        const enabeLog = true; //日志开关。可填值：true、


        $BodyType="json";
        $ServerIP='app.cloopen.com';
        $ServerPort='8883';
        $SoftVersion='2013-12-26';
        $AccountSid='8aaf070863f8fb0401641c2cec29192d';
        $Batch=date("YmdHis");
        $AppId= '8aaf070863f8fb0401641c2cec7d1933';
        $AccountToken='ec40ffdc5e7644c8a248d6f363009b45';

        if($BodyType=="json"){
            $data="";
            for($i=0;$i<count($datas);$i++){
                $data = $data. "'".$datas[$i]."',";
            }

            $body= "{'to':'$to','templateId':'$tempId','appId':$AppId,'datas':[".$data."]}";
            //'appId':'$this->AppId',
        }else{
            $data="";
            for($i=0;$i<count($datas);$i++){
                $data = $data. "<data>".$datas[$i]."</data>";
            }
            $body="<TemplateSMS>
                                        <to>$to</to>
                                        <appId>$AppId</appId>
                                        <templateId>$tempId</templateId>
                                        <datas>".$data."</datas>
                                    </TemplateSMS>";
        }

//        $this->showlog($body);
        // 大写的sig参数
        $sig =  strtoupper(md5($AccountSid . $AccountToken . $Batch));

        // 生成请求URL
        $url="https://$ServerIP:$ServerPort/$SoftVersion/Accounts/$AccountSid/SMS/TemplateSMS?sig=$sig";
//        $this->showlog("request url = ".$url);
        // 生成授权：主帐户Id + 英文冒号 + 时间戳。
        $authen = base64_encode($AccountSid . ":" . $Batch);

//        // 生成包头
//        $header = "";
//        $header->Accept='application/json';

          $header = [
              'Accept:application/json',
              'Content-Type:application/json;charset=utf-8',
              'Authorization:'.$authen.''
          ];

//        $header = array("Accept:application/$BodyType","Content-Type:application/$BodyType;charset=utf-8","Authorization:$authen");

        //,"Content-Type:application/json;charset=utf-8"
        //,"Authorization:$authen"
//        // 发送请求

        $result = $this->curl_post($url,$body,$header);

//        $this->showlog("response body = ".$result);
//        if($BodyType=="json"){//JSON格式
//            $datas=json_decode($result);
//        }else{ //xml格式
//            $datas = simplexml_load_string(trim($result," \t\n\r"));
//        }
//          if($datas == FALSE){
//            $datas = new stdClass();
//            $datas->statusCode = '172003';
//            $datas->statusMsg = '返回包体错误';
//        }
//        //重新装填数据
//        if($datas->statusCode==0){
//            if($BodyType=="json"){
//                $datas->TemplateSMS =$datas->templateSMS;
//                unset($datas->templateSMS);
//            }
//        }
//        print_r($header);
        return $result;
    }


    /**
     * 主帐号鉴权
     */
    public function accAuth()
    {
        if($this->ServerIP==""){
            $data = new stdClass();
            $data->statusCode = '172004';
            $data->statusMsg = 'IP为空';
            return $data;
        }
        if($this->ServerPort<=0){
            $data = new stdClass();
            $data->statusCode = '172005';
            $data->statusMsg = '端口错误（小于等于0）';
            return $data;
        }
        if($this->SoftVersion==""){
            $data = new stdClass();
            $data->statusCode = '172013';
            $data->statusMsg = '版本号为空';
            return $data;
        }
        if($this->AccountSid==""){
            $data = new stdClass();
            $data->statusCode = '172006';
            $data->statusMsg = '主帐号为空';
            return $data;
        }
        if($this->AccountToken==""){
            $data = new stdClass();
            $data->statusCode = '172007';
            $data->statusMsg = '主帐号令牌为空';
            return $data;
        }
        if($this->AppId==""){
            $data = new stdClass();
            $data->statusCode = '172012';
            $data->statusMsg = '应用ID为空';
            return $data;
        }
    }
}